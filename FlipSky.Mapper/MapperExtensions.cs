﻿using System;
using System.Linq;
using System.Collections.Generic;
using FlipSky.Ensures;

namespace FlipSky.Mapper
{
    /// <summary>
    /// Extension methods for object mapping
    /// </summary>
    public static class MapperExtensions
    {
        /// <summary>
        /// Map a single instance. If the source is already of the correct type it will not
        /// be modified.
        /// </summary>
        public static TTarget Map<TSource, TTarget>(
            this IMapper<TSource, TTarget> mapper,
            TSource source
            ) where TTarget : class
        {
            Ensure.IsNotNull<ArgumentNullException>(mapper, nameof(mapper));
            // Create the new instance and do the mapping
            var target = mapper.CreateInstance(source);
            if (target != null)
                mapper.Map(source, target);
            return target;
        }

        /// <summary>
        /// Map an enumeration to the desired type
        /// </summary>
        public static IEnumerable<TTarget> Map<TSource, TTarget>(
            this IMapper<TSource, TTarget> mapper,
            IEnumerable<TSource> source
            ) where TTarget : class
        {
            Ensure.IsNotNull<ArgumentNullException>(mapper, nameof(mapper));
            return source.Select(o =>
            {
                // Create the new instance and do the mapping
                var target = mapper.CreateInstance(o);
                if (target != null)
                    mapper.Map(o, target);
                return target;
            });
        }

        /// <summary>
        /// Helper for generic lists
        /// </summary>
        public static IList<TTarget> Map<TSource, TTarget>(
            this IMapper<TSource, TTarget> mapper,
            IReadOnlyList<TSource> source
            ) where TTarget : class
        {
            return mapper.Map((IEnumerable<TSource>)source)
                .ToList();
        }

        /// <summary>
        /// Helper for arrays
        /// </summary>
        public static TTarget[] Map<TSource, TTarget>(
            this IMapper<TSource, TTarget> mapper,
            TSource[] source
            ) where TTarget : class
        {
            return mapper.Map((IEnumerable<TSource>)source)
                .ToArray();
        }
    }
}
