﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.Mapper
{
    /// <summary>
    /// An abstract implementation of IMapper with a factory method
    /// that will create an instance using the parameterless public
    /// constructor.
    /// </summary>
    public abstract class AbstractMapper<TSource, TTarget> : IMapper<TSource, TTarget>
        where TTarget : class, new()
    {
        /// <summary>
        /// If set the mapper will not create a new instance if the
        /// source is already the same type as the target.
        /// </summary>
        public bool DoNotClone { get; private set; }

        /// <summary>
        /// Map from source instance to target instance
        /// </summary>
        public abstract void Map(TSource source, TTarget target);

        /// <summary>
        /// Create a new instance of the target class using the default
        /// factory
        /// </summary>
        public TTarget CreateInstance(TSource source)
        {
            // Null always maps to null
            if (source == null)
                return null;
            // Only create an instance if we need to and are allowed
            if (DoNotClone && source is TTarget target)
                return target;
            return Factory<TTarget>.CreateInstance();
        }

        /// <summary>
        /// Constructor with clone control
        /// </summary>
        public AbstractMapper(bool doNotClone = true)
        {
            DoNotClone = doNotClone;
        }
    }
}
