﻿namespace FlipSky.Mapper
{
    /// <summary>
    /// The simplest possible interface to map one instance to another
    /// </summary>
    public interface IMapper<TSource, TTarget> where TTarget : class
    {
        /// <summary>
        /// Populate instances of target with values from source.
        /// </summary>
        void Map(TSource source, TTarget target);

        /// <summary>
        /// Create a new instance, can use properties of source as a
        /// reference for creation. Do not use this method to map.
        /// </summary>
        TTarget CreateInstance(TSource source);
    }
}
