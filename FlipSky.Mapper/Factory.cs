﻿using System;
using System.Linq.Expressions;

namespace FlipSky.Mapper
{
    /// <summary>
    /// A generic object factory
    /// </summary>
    internal static class Factory<T> where T : new()
    {
        /// <summary>
        /// Generate and compile the factory method
        /// </summary>
        private static readonly Func<T> CreateInstanceFunc =
            Expression.Lambda<Func<T>>(Expression.New(typeof(T))).Compile();

        /// <summary>
        /// Creates an instance of type T by calling it's parameterless constructor.
        /// </summary>
        public static T CreateInstance() => CreateInstanceFunc();
    }
}
