﻿using System;
using FlipSky.Ensures;
using FlipSky.Strings;

namespace FlipSky.Options
{
    /// <summary>
    /// Support for parsing command line options
    /// </summary>
    public class OptionParser
    {
        /// <summary>
        /// The set of options defined for this parser
        /// </summary>
        private readonly Implementation.OptionList Options = new Implementation.OptionList();

        /// <summary>
        /// Add an option with a short name and a long name
        /// </summary>
        public OptionDetail AddOption(char shortName, string longName)
        {
            Ensure.IsTrue<ArgumentException>(char.IsLetter(shortName));
            Ensure.IsTrue<ArgumentException>(longName.IsIdentifier(allowDashes: true));
            return Options.Add(new OptionDetail(shortName, longName));
        }

        /// <summary>
        /// Add an option with a short name only
        /// </summary>
        public OptionDetail AddOption(char shortName)
        {
            Ensure.IsTrue<ArgumentException>(char.IsLetter(shortName));
            return Options.Add(new OptionDetail(shortName, null));
        }

        /// <summary>
        /// Add an option with a long name only
        /// </summary>
        public OptionDetail AddOption(string longName)
        {
            Ensure.IsTrue<ArgumentException>(longName.IsIdentifier(allowDashes: true));
            return Options.Add(new OptionDetail(null, longName));
        }

        /// <summary>
        /// Parse the command line for options and return an array of all remaining
        /// non-option arguments.
        /// </summary>
        public string[] Parse(string[] args)
        {
            return Options.Parse(args);
        }

        /// <summary>
        /// Generate a help string for the current options
        /// </summary>
        public string GenerateOptionHelp(int width = 80)
        {
            return Options.GenerateOptionHelp(width);
        }
    }
}
