﻿namespace FlipSky.Options.Implementation
{
    /// <summary>
    /// Value requirements for options
    /// </summary>
    internal enum ValueType
    {
        None,     // Option has no value
        Optional, // Value is optional
        Required  // Value is required
    }
}
