﻿using FlipSky.Ensures;

namespace FlipSky.Options.Implementation
{
    internal class ArgumentValue
    {
        //--- Indicators
        private const string LongOptionIdentifier = "--";
        private const string ShortOptionIdentifier = "-";
        private const string EndOfOptionsIdentifier = "--";

        /// <summary>
        /// The type of the argument
        /// </summary>
        public ArgumentType Type { get; private set; }

        /// <summary>
        /// The value of the argument (the option name or the argument value)
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// The name (only valid for option arguments)
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Constructor with detection
        /// </summary>
        public ArgumentValue(string arg, bool forceArgument = false)
        {
            Value = arg;
            if (forceArgument)
                Type = ArgumentType.Argument;
            else
            {
                // Check for end of options
                if (arg == EndOfOptionsIdentifier)
                    Type = ArgumentType.EndOfOptions;
                else if (arg.StartsWith(LongOptionIdentifier))
                {
                    Type = ArgumentType.LongOption;
                    Name = arg.Substring(LongOptionIdentifier.Length).ToLower();
                }
                else if (arg.StartsWith(ShortOptionIdentifier))
                {
                    if (arg.Length == 1)
                        Type = ArgumentType.Argument;
                    else
                    {
                        Ensure.IsTrue<OptionException>(arg.Length == 2, $"Option is incorrectly formatted - '{arg}'");
                        Type = ArgumentType.ShortOption;
                        Name = arg.Substring(1);
                    }
                }
                else
                    Type = ArgumentType.Argument;
            }
        }
    }
}
