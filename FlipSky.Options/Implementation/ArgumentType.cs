﻿namespace FlipSky.Options.Implementation
{
    /// <summary>
    /// Types of arguments we will find
    /// </summary>
    internal enum ArgumentType
    {
        ShortOption,   // A short option
        LongOption,    // A long option
        EndOfOptions,  // End of option list
        Argument       // An argument (not an option)
    }
}
