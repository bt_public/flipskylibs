﻿using System.Linq;
using System.Collections.Generic;

namespace FlipSky.Options.Implementation
{
    internal class CommandLine
    {
        // List of arguments
        private readonly List<ArgumentValue> Arguments = new List<ArgumentValue>();
        private int Index = 0;

        /// <summary>
        /// Constructor with an array of arguments
        /// </summary>
        /// <param name="args"></param>
        public CommandLine(string[] args)
        {
            // Categorise the arguments
            bool endOfOptionsFound = false;
            foreach (var arg in args)
            {
                var a = new ArgumentValue(arg, forceArgument: endOfOptionsFound);
                Arguments.Add(a);
                if (a.Type == ArgumentType.EndOfOptions)
                    endOfOptionsFound = true;
            }
        }

        /// <summary>
        /// Test if another argument is available
        /// </summary>
        public bool HasNext()
        {
            return Index < Arguments.Count;
        }

        /// <summary>
        /// Fetch, but do not remove, the next argument
        /// </summary>
        public ArgumentValue Peek()
        {
            return Arguments[Index];
        }

        /// <summary>
        /// Fetch, but do not remove, the next argument. If no argument is
        /// available it will return false.
        /// </summary>
        public bool TryPeek(out ArgumentValue value)
        {
            value = null;
            if (Index >= Arguments.Count)
                return false;
            value = Arguments[Index];
            return true;
        }

        /// <summary>
        /// Fetch the next argument
        /// </summary>
        public ArgumentValue Next()
        {
            var value = Arguments[Index];
            Index = Index + 1;
            return value;
        }

        /// <summary>
        /// Ensure that all remaining values are arguments
        /// </summary>
        public bool VerifyRemaining()
        {
            for (int i = Index; i < Arguments.Count; i++)
                if (Arguments[i].Type != ArgumentType.Argument)
                    return false;
            return true;
        }

        /// <summary>
        /// Get the remaining arguments as an array of values
        /// </summary>
        public string[] GetRemainingArguments()
        {
            return Arguments.Skip(Index).Select(a => a.Value).ToArray();
        }
    }
}
