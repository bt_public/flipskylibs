﻿using System.Linq;
using System.Collections.Generic;
using FlipSky.Ensures;
using FlipSky.Strings;

namespace FlipSky.Options.Implementation
{
    /// <summary>
    /// Manages the list of options defined for the parser
    /// </summary>
    internal class OptionList
    {
        //--- The set of options defined
        private List<OptionDetail> Options = new List<OptionDetail>();

        /// <summary>
        /// Find an option by it's short name
        /// </summary>
        private OptionDetail FindByShortName(char shortName)
        {
            return Options.Where(o => o.ShortName.HasValue && (o.ShortName.Value == shortName)).FirstOrDefault();
        }

        /// <summary>
        /// Find an option by it's long name
        /// </summary>
        private OptionDetail FindByLongName(string longName)
        {
            return Options.Where(o => o.LongName == longName).FirstOrDefault();
        }

        /// <summary>
        /// Add an option to the list. Will throw an exception if the option
        /// is already defined by it's short name or long name.
        /// </summary>
        public OptionDetail Add(OptionDetail option)
        {
            if (option.ShortName.HasValue && (FindByShortName(option.ShortName.Value) != null))
                throw new OptionException($"An option has already been defined for the character '{option.ShortName.Value}'");
            if (FindByLongName(option.LongName) != null)
                throw new OptionException($"An option has already been defined for the name '{option.LongName}'");
            Options.Add(option);
            return option;
        }

        /// <summary>
        /// Do the actual processing
        /// </summary>
        public string[] Parse(string[] args)
        {
            var optionsSeen = new HashSet<OptionDetail>();
            var commandLine = new CommandLine(args);
            // Walk through the args
            bool verifyRemaining = true;
            while (commandLine.HasNext())
            {
                var arg = commandLine.Peek();
                if (arg.Type == ArgumentType.Argument)
                    break; // Stop and leave it behind
                commandLine.Next();
                // If it is end of options we can stop as well
                if (arg.Type == ArgumentType.EndOfOptions)
                {
                    verifyRemaining = false;
                    break;
                }
                // Get the option
                var option = (arg.Type == ArgumentType.LongOption) ?
                    FindByLongName(arg.Name) :
                    FindByShortName(arg.Name[0]);
                Ensure.IsNotNull<OptionException>(option, $"Undefined option '{arg.Value}'");
                // If it is a singleton make sure we haven't seen it yet
                Ensure.IsFalse<OptionException>(option.Singleton && optionsSeen.Contains(option), $"Option '{arg.Value}' may only be used once.");
                // See if the option requires (or supports) a parameter
                ArgumentValue value = null;
                if (option.Value == ValueType.Optional)
                {
                    if (commandLine.TryPeek(out value) && (value.Type == ArgumentType.Argument))
                        commandLine.Next(); // Consume it
                    else
                        value = null; // Another option or indicator
                }
                else if (option.Value == ValueType.Required)
                {
                    if (!(commandLine.TryPeek(out value) && (value.Type == ArgumentType.Argument)))
                        throw new OptionException($"A value is required for option '{arg.Value}'");
                    commandLine.Next();
                }
                // Invoke the action and remember that we have seen this option
                option.ValueAction?.Invoke(value == null ? null : value.Value);
                optionsSeen.Add(option);
            }
            // Verify and return the rest of the arguments
            if (verifyRemaining && !commandLine.VerifyRemaining())
                throw new OptionException("One or more options appear after the first argument.");
            return commandLine.GetRemainingArguments();
        }

        /// <summary>
        /// Generate a help string for the current options
        /// </summary>
        public string GenerateOptionHelp(int width)
        {
            if (Options.Count == 0)
                return "There are no options available.";
            // Build a list of 'friendly' option names
            var names = Options.Select(o => o.ToString());
            // Figure out our padding (maximum name length + 2)
            var padding = names.Select(n => n.Length).Max() + 2;
            // And combine it all together
            return string.Join("\n", names.Zip(Options, (n, o) =>
            {
                var desc = StringHelpers.LayoutText(
                    o.Description ?? "No description provided for this option.",
                    width,
                    padding
                    );
                // Replace the first part of the string with the friendly name
                return n + desc.Substring(n.Length);
            }));
        }

    }
}
