﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace FlipSky.Options
{
    /// <summary>
    /// Exceptions thrown by the option parser
    /// </summary>
    public class OptionException : Exception
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        [ExcludeFromCodeCoverage]
        public OptionException() { }

        /// <summary>
        /// Constructor with message
        /// </summary>
        [ExcludeFromCodeCoverage]
        public OptionException(string message) : base(message) { }

        /// <summary>
        /// Constructor with message and cause
        /// </summary>
        [ExcludeFromCodeCoverage]
        public OptionException(string message, Exception innerException) : base(message, innerException) { }
    }
}
