﻿using System;
using System.Text;
using FlipSky.Ensures;

namespace FlipSky.Options
{
    /// <summary>
    /// Represents information about a single option. Provides a fluent
    /// interface to adjust option settings.
    /// </summary>
    public class OptionDetail : IEquatable<OptionDetail>
    {
        /// <summary>
        /// The short name of the option
        /// </summary>
        public char? ShortName { get; private set; }

        /// <summary>
        /// The long name of the option
        /// </summary>
        public string LongName { get; private set; }

        /// <summary>
        /// Description of the option
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Value requirements
        /// </summary>
        internal Implementation.ValueType Value { get; private set; }

        /// <summary>
        /// Is the option only allowed once
        /// </summary>
        internal bool Singleton { get; private set; } = true;

        /// <summary>
        /// The action to invoke when the option is found
        /// </summary>
        internal Action<string> ValueAction { get; private set; }

        /// <summary>
        /// Constructor with basic required information
        /// </summary>
        internal OptionDetail(char? shortName, string longName)
        {
            ShortName = shortName;
            LongName = longName?.ToLower();
        }

        #region Fluent API
        /// <summary>
        /// Allow this option to be used more than once
        /// </summary>
        public OptionDetail AllowMoreThanOnce()
        {
            Singleton = false;
            return this;
        }

        /// <summary>
        /// Option has a non-mandatory value
        /// </summary>
        public OptionDetail WithOptionalValue()
        {
            Value = Implementation.ValueType.Optional;
            return this;
        }

        /// <summary>
        /// Option requires a value
        /// </summary>
        public OptionDetail WithRequiredValue()
        {
            Value = Implementation.ValueType.Required;
            return this;
        }

        /// <summary>
        /// Set the description. This will fail if the description is
        /// already set.
        /// </summary>
        public OptionDetail WithDescription(string description)
        {
            Ensure.IsNotNullOrWhiteSpace<ArgumentException>(description);
            Ensure.IsNull<InvalidOperationException>(Description);
            Description = description;
            return this;
        }

        /// <summary>
        /// Set the action to perform when the option is detected. Will fail
        /// if an action has already been set.
        /// </summary>
        public OptionDetail WithAction(Action<string> action)
        {
            Ensure.IsNotNull<ArgumentException>(action);
            Ensure.IsNull<InvalidOperationException>(ValueAction);
            ValueAction = action;
            return this;
        }
        #endregion

        #region Equality Testing
        /// <summary>
        /// Test against another instance
        /// </summary>
        public bool Equals(OptionDetail other)
        {
            return other != null &&
                   other.ShortName == ShortName &&
                   other.LongName == LongName;
        }

        /// <summary>
        /// Test against an arbitrary object
        /// </summary>
        public override bool Equals(object obj)
        {
            return Equals(obj as OptionDetail);
        }

        /// <summary>
        /// Get a hash for this instance
        /// </summary>
        public override int GetHashCode()
        {
            if (LongName != null)
                return LongName.GetHashCode();
            return ShortName.Value.GetHashCode();
        }
        #endregion

        /// <summary>
        /// Return a string representation of the option
        /// </summary>
        public override string ToString()
        {
            var result = new StringBuilder();
            // Add the option selectors
            if (ShortName.HasValue && (LongName != null))
                result.Append($"-{ShortName.Value}|--{LongName}");
            else if (ShortName.HasValue)
                result.Append($"-{ShortName.Value}");
            else
                result.Append($"--{LongName}");
            // Add the value component
            if (Value == Implementation.ValueType.Required)
                result.Append(" value");
            else if (Value == Implementation.ValueType.Optional)
                result.Append(" [value]");
            // And done
            return result.ToString();
        }
    }
}
