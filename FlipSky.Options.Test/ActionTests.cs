﻿using Xunit;

namespace FlipSky.Options.Test
{
    /// <summary>
    /// Test that common use cases behave as expected
    /// </summary>
    public class ActionTests
    {
        /// <summary>
        /// Test class to manage options
        /// </summary>
        private class TestOptions : OptionParser
        {
            /// <summary>
            /// A flag option, no argument
            /// </summary>
            public bool FlagOption { get; set; }

            /// <summary>
            /// An option without a required argument
            /// </summary>
            public string OptionalArgument { get; set; }

            /// <summary>
            /// If we saw the option above
            /// </summary>
            public bool OptionalArgumentSeen { get; set; }

            /// <summary>
            /// An option with a required argument
            /// </summary>
            public string RequiredArgument { get; set; }

            public TestOptions()
            {
                AddOption('f', "flag")
                    .WithAction((v) => FlagOption = true);
                AddOption('o', "optional")
                    .WithAction((v) => { OptionalArgumentSeen = true; OptionalArgument = v; })
                    .WithOptionalValue();
                AddOption('r', "required")
                    .WithAction((v) => RequiredArgument = v)
                    .WithRequiredValue();
            }
        }

        /// <summary>
        /// Test common usage patterns
        /// </summary>
        [Theory]
        // Flag option testing
        [InlineData(true, null, false, null, 0, new string[] { "-f" })]
        [InlineData(true, null, false, null, 1, new string[] { "-f", "value" })]
        [InlineData(true, null, false, null, 0, new string[] { "--flag" })]
        [InlineData(true, null, false, null, 1, new string[] { "--flag", "value" })]
        // Optional value testing
        [InlineData(false, "value", true, null, 0, new string[] { "-o", "value" })]
        [InlineData(false, null, true, null, 0, new string[] { "-o" })]
        [InlineData(false, "value", true, null, 0, new string[] { "--optional", "value" })]
        [InlineData(false, null, true, null, 0, new string[] { "--optional" })]
        [InlineData(false, null, true, null, 1, new string[] { "-o", "--", "value" })]
        [InlineData(false, null, true, null, 1, new string[] { "--optional", "--", "value" })]
        // Dash is a valid option
        [InlineData(true, "-", true, null, 0, new string[] { "-o", "-", "-f" })]
        [InlineData(true, "-", true, null, 0, new string[] { "--optional", "-", "-f" })]
        [InlineData(true, null, false, "-", 0, new string[] { "-r", "-", "-f" })]
        [InlineData(true, null, false, "-", 0, new string[] { "--required", "-", "-f" })]
        public void CommonUsagePatternsSucceed(bool flag, string optional, bool optionalSeen, string required, int remaining, string[] cmdArgs)
        {
            var parser = new TestOptions();
            var args = parser.Parse(cmdArgs);
            Assert.Equal(flag, parser.FlagOption);
            Assert.Equal(optional, parser.OptionalArgument);
            Assert.Equal(optionalSeen, parser.OptionalArgumentSeen);
            Assert.Equal(required, parser.RequiredArgument);
            Assert.Equal(remaining, args.Length);
        }
    }
}
