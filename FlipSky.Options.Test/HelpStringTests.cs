﻿using Xunit;

namespace FlipSky.Options.Test
{
    /// <summary>
    /// Verify the help text generation for options
    /// </summary>
    public class HelpStringTests
    {
        [Fact]
        public void WillGenerateTextWithNoOptions()
        {
            var parser = new OptionParser();
            var help = parser.GenerateOptionHelp();
            Assert.NotNull(help);
        }

        [Theory]
        [InlineData('a', "Option A", false, false, "-a  Option A")]
        [InlineData('a', "Option A", true, false, "-a value  Option A")]
        [InlineData('a', "Option A", false, true, "-a [value]  Option A")]
        public void WillGenerateCorrectTextForShortOptions(char shortName, string description, bool required, bool optional, string expected)
        {
            var parser = new OptionParser();
            var option = parser.AddOption(shortName)
                .WithDescription(description);
            if (required)
                option = option.WithRequiredValue();
            if (optional)
                option = option.WithOptionalValue();
            var help = parser.GenerateOptionHelp();
            // Verify options without parameters are present
            Assert.Contains(expected, help);
        }

        [Theory]
        [InlineData("option", "Option A", false, false, "--option  Option A")]
        [InlineData("option", "Option A", true, false, "--option value  Option A")]
        [InlineData("option", "Option A", false, true, "--option [value]  Option A")]
        public void WillGenerateCorrectTextForLongOptions(string longName, string description, bool required, bool optional, string expected)
        {
            var parser = new OptionParser();
            var option = parser.AddOption(longName)
                .WithDescription(description);
            if (required)
                option = option.WithRequiredValue();
            if (optional)
                option = option.WithOptionalValue();
            var help = parser.GenerateOptionHelp();
            // Verify options without parameters are present
            Assert.Contains(expected, help);
        }

        [Theory]
        [InlineData('a', "option", "Option A", false, false, "-a|--option  Option A")]
        [InlineData('a', "option", "Option A", true, false, "-a|--option value  Option A")]
        [InlineData('a', "option", "Option A", false, true, "-a|--option [value]  Option A")]
        public void WillGenerateCorrectTextForBothOptions(char shortName, string longName, string description, bool required, bool optional, string expected)
        {
            var parser = new OptionParser();
            var option = parser.AddOption(shortName, longName)
                .WithDescription(description);
            if (required)
                option = option.WithRequiredValue();
            if (optional)
                option = option.WithOptionalValue();
            var help = parser.GenerateOptionHelp();
            // Verify options without parameters are present
            Assert.Contains(expected, help);
        }

        [Theory]
        [InlineData('a', "option", "This is a very long description.", false, false, "-a|--option  This is a very long\n")]
        [InlineData('a', "option", "This is a very long description.", true, false, "-a|--option value  This is a very long\n")]
        [InlineData('a', "option", "This is a very long description.", false, true, "-a|--option [value]  This is a very long\n")]
        public void WillWrapLinesToMatchWidth(char shortName, string longName, string description, bool required, bool optional, string expected)
        {
            var parser = new OptionParser();
            var option = parser.AddOption(shortName, longName)
                .WithDescription(description);
            if (required)
                option = option.WithRequiredValue();
            if (optional)
                option = option.WithOptionalValue();
            var help = parser.GenerateOptionHelp(40);
            // Verify options without parameters are present
            Assert.Contains(expected, help);
        }

    }
}
