﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FlipSky.Options.Test
{
    /// <summary>
    /// Verify option construction
    /// </summary>
    public class ConstructionTests
    {
        [Theory]
        [InlineData('a')]
        public void WillCreateValidShortOptions(char shortName)
        {
            var parser = new OptionParser();
            Assert.NotNull(parser.AddOption(shortName));
        }

        [Theory]
        [InlineData("long")]
        [InlineData("long-option")]
        public void WillCreateValidLongOptions(string longName)
        {
            var parser = new OptionParser();
            Assert.NotNull(parser.AddOption(longName));
        }

        [Theory]
        [InlineData('o', "option")]
        [InlineData('o', "long-option")]
        public void WillCreateValidOptions(char shortName, string longName)
        {
            var parser = new OptionParser();
            Assert.NotNull(parser.AddOption(shortName, longName));
        }

        [Theory]
        [InlineData('-')]
        [InlineData(' ')]
        [InlineData('$')]
        [InlineData('#')]
        public void WillFailWithInvalidShortOption(char shortName)
        {
            var parser = new OptionParser();
            Assert.Throws<ArgumentException>(() => parser.AddOption(shortName));
        }

        [Theory]
        [InlineData("contains spaces")]
        [InlineData("99numbers")]
        [InlineData(" startsWithSpace")]
        public void WillFailWithInvalidLongOption(string longName)
        {
            var parser = new OptionParser();
            Assert.Throws<ArgumentException>(() => parser.AddOption(longName));
        }

        [Theory]
        [InlineData('a', "aoption", 'a', "aoption")]
        [InlineData('a', "aoption", 'b', "aoption")]
        [InlineData('a', "aoption", 'a', "boption")]
        public void CannotAddOptionsWithSameName(char s1, string l1, char s2, string l2)
        {
            var parser = new OptionParser();
            parser.AddOption(s1, l1);
            Assert.Throws<OptionException>(() => parser.AddOption(s2, l2));
        }
    }
}
