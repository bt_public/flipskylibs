﻿using Xunit;

namespace FlipSky.Options.Test
{
    public class ParsingTests
    {
        /// <summary>
        /// Set up an instance to test with
        /// </summary>
        private OptionParser SetUpTestInstance()
        {
            var parser = new OptionParser();
            parser.AddOption('f', "flag")
                .WithDescription("Flag option with no argument");
            parser.AddOption('o', "optional")
                .WithDescription("Has an optional argument")
                .WithOptionalValue();
            parser.AddOption('r', "required")
                .WithDescription("Has a required argument")
                .WithRequiredValue();
            parser.AddOption('m', "multiple")
                .WithDescription("May appear multiple times")
                .AllowMoreThanOnce();
            // Done
            return parser;
        }

        /// <summary>
        /// Check valid command lines against the template
        /// </summary>
        [Theory]
        [InlineData(new string[] { "-f", "-o", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "-o", "optional", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "--flag", "-o", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "--flag", "-o", "optional", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "--optional", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "--optional", "optional", "-r", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "-o", "--required", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "-o", "optional", "--required", "value", "-m" }, 0)]
        [InlineData(new string[] { "-f", "-o", "-r", "value", "--multiple" }, 0)]
        [InlineData(new string[] { "-f", "-o", "optional", "-r", "value", "--multiple" }, 0)]
        // Value after options will be passed as arguments
        [InlineData(new string[] { "-f", "value" }, 1)]
        [InlineData(new string[] { "--flag", "value" }, 1)]
        [InlineData(new string[] { "-r", "value", "other" }, 1)]
        [InlineData(new string[] { "--required", "value", "other" }, 1)]
        // Optional argument at end of option list will favour option
        [InlineData(new string[] { "-o", "value" }, 0)]
        [InlineData(new string[] { "-o", "--", "value" }, 1)]
        [InlineData(new string[] { "--optional", "value" }, 0)]
        [InlineData(new string[] { "--optional", "--", "value" }, 1)]
        // Multiple values are allowed for specific arguments
        [InlineData(new string[] { "-f", "-m", "-o", "-m" }, 0)]
        [InlineData(new string[] { "-f", "--multiple", "-o", "-m" }, 0)]
        [InlineData(new string[] { "-f", "--multiple", "-o", "--multiple" }, 0)]
        public void WillParseValidCommandLine(string[] cmdArgs, int remaining)
        {
            var parser = SetUpTestInstance();
            var arguments = parser.Parse(cmdArgs);
            Assert.Equal(remaining, arguments.Length);
        }

        /// <summary>
        /// Invalid command lines will cause an OptionException
        /// </summary>
        [Theory]
        // Required option without parameter
        [InlineData("-r")]
        [InlineData("-r", "-o")]
        [InlineData("--required")]
        [InlineData("--required", "-o")]
        // Flag parameter with option stops option processing
        [InlineData("-f", "value", "-o")]
        [InlineData("--flag", "value", "-o")]
        // End of options marker after last option will fail
        [InlineData("-f", "value", "--", "other")]
        // Multiple options will fail
        [InlineData("-f", "-f")]
        [InlineData("--flag", "-f")]
        [InlineData("-f", "--flag")]
        [InlineData("--flag", "--flage")]
        public void WillFailInvalidCommandLine(params string[] cmdArgs)
        {
            var parser = SetUpTestInstance();
            Assert.Throws<OptionException>(() => parser.Parse(cmdArgs));
        }

        /// <summary>
        /// The remainder of the command line is correctly returned
        /// </summary>
        [Theory]
        [InlineData(new string[] { "-f", "v1", "v2" }, new string[] { "v1", "v2" })]
        [InlineData(new string[] { "--flag", "v1", "v2" }, new string[] { "v1", "v2" })]
        [InlineData(new string[] { "-r", "v1", "v2" }, new string[] { "v2" })]
        [InlineData(new string[] { "--required", "v1", "v2" }, new string[] { "v2" })]
        [InlineData(new string[] { "-o", "v1", "v2" }, new string[] { "v2" })]
        [InlineData(new string[] { "--optional", "v1", "v2" }, new string[] { "v2" })]
        [InlineData(new string[] { "-o", "--", "v1", "v2" }, new string[] { "v1", "v2" })]
        [InlineData(new string[] { "--optional", "--", "v1", "v2" }, new string[] { "v1", "v2" })]
        [InlineData(new string[] { "-o", "--", "-v1", "v2" }, new string[] { "-v1", "v2" })]
        [InlineData(new string[] { "--optional", "--", "-v1", "v2" }, new string[] { "-v1", "v2" })]
        [InlineData(new string[] { "-o", "-f", "v1", "v2" }, new string[] { "v1", "v2" })]
        [InlineData(new string[] { "--optional", "-f", "v1", "v2" }, new string[] { "v1", "v2" })]
        public void WillGetLeftoverArguments(string[] cmdArgs, string[] expected)
        {
            var parser = SetUpTestInstance();
            var args = parser.Parse(cmdArgs);
            Assert.Equal(expected.Length, args.Length);
            for (int i = 0; i < expected.Length; i++)
                Assert.Equal(expected[i], args[i]);
        }
    }
}
