# FlipSky Common Libraries

A collection of small utility libraries for .NET Standard with a [GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html) (see [LICENSE.txt](license.txt)).

Over time I have written a number of small, single purpose libraries to help with my own internal projects and to learn how various patterns work internally. Until now these have been stashed away in private repositories. Now that I want to make some of the projects that depend on these libraries public I have created this public repository to hold the code for them and to publish them to NuGet.

While some of these duplicate functionality of other popular libraries they tend to be smaller, more focused on a specific task and integrate with each other well. I have found that using these has greatly increased my productivity when it comes to developing new projects. I hope they are as useful to you.
