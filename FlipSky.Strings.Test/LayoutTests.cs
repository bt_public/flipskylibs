﻿using System;
using Xunit;

namespace FlipSky.Strings.Test
{
    public class LayoutTests
    {
        // Sample text
        private const string LoremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec lacus odio. Praesent luctus et massa ut viverra.";

        // Sample results
        private const string Width40Padding0NewLine = "Lorem ipsum dolor sit amet, consectetur\nadipiscing elit. Nullam nec lacus odio.\nPraesent luctus et massa ut viverra.";
        private const string Width40Padding4NewLine = "    Lorem ipsum dolor sit amet,\n    consectetur adipiscing elit. Nullam\n    nec lacus odio. Praesent luctus et\n    massa ut viverra.";
        private const string Width40Padding0CRLF = "Lorem ipsum dolor sit amet, consectetur\r\nadipiscing elit. Nullam nec lacus odio.\r\nPraesent luctus et massa ut viverra.";
        private const string Width40Padding4CRLF = "    Lorem ipsum dolor sit amet,\r\n    consectetur adipiscing elit. Nullam\r\n    nec lacus odio. Praesent luctus et\r\n    massa ut viverra.";

        /// <summary>
        /// Verify the layout is done correctly
        /// </summary>
        [Theory]
        [InlineData(LoremIpsum, 40, 0, "\n", Width40Padding0NewLine)]
        [InlineData(LoremIpsum, 40, 0, "\r\n", Width40Padding0CRLF)]
        [InlineData(LoremIpsum, 40, 4, "\n", Width40Padding4NewLine)]
        [InlineData(LoremIpsum, 40, 4, "\r\n", Width40Padding4CRLF)]
        public void WillCorrectlyLayout(string source, int width, int padding, string eol, string expected)
        {
            var result = StringHelpers.LayoutText(source, width, padding, eol);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(0, 0, "\n")]
        [InlineData(-5, 0, "\n")]
        [InlineData(40, -2, "\n")]
        [InlineData(40, 40, "\n")]
        [InlineData(0, 0, null)]
        [InlineData(-5, 0, null)]
        [InlineData(40, -2, null)]
        [InlineData(40, 40, null)]
        [InlineData(40, 0, null)]
        public void WillFailWithInvalidArguments(int width, int padding, string eol)
        {
            Assert.Throws<ArgumentException>(
                () => StringHelpers.LayoutText(LoremIpsum, width, padding, eol)
                );
        }

        /// <summary>
        /// Verify layout fails if a word cannot fit. This depends on the sample
        /// text having at least one word more than 10 charactes long
        /// </summary>
        [Theory]
        [InlineData(10, 0)]
        [InlineData(40, 30)]
        public void WillFailIfNotEnoughSpace(int width, int padding)
        {
            Assert.Throws<InvalidOperationException>(
                () => StringHelpers.LayoutText(LoremIpsum, width, padding)
                );
        }
    }
}
