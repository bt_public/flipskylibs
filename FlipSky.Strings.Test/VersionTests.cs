﻿using Xunit;

namespace FlipSky.Strings.Test
{
    /// <summary>
    /// Verify version matching
    /// </summary>
    public class VersionTests
    {
        /// <summary>
        /// Valid version numbers will match
        /// </summary>
        [Theory]
        [InlineData("1.0")]
        [InlineData("1.0.1")]
        [InlineData("0.0")]
        [InlineData("0.0.1")]
        [InlineData("0.10.13")]
        [InlineData("123.456")]
        [InlineData("123.456.789")]
        public void WillMatchValidVersion(string version)
        {
            Assert.True(version.IsVersionNumber());
        }

        /// <summary>
        /// Invalid version numbers will not match
        /// </summary>
        [Theory]
        [InlineData("1")]
        [InlineData("01.2")]
        [InlineData("01.2.3")]
        [InlineData("1.2-preview")]
        [InlineData("1.2.3-preview")]
        [InlineData("1.2 3")]
        [InlineData("")]
        [InlineData(null)]
        public void WillNotMatchInvalidVersion(string version)
        {
            Assert.False(version.IsVersionNumber());
        }

        /// <summary>
        /// Valid version numbers will match
        /// </summary>
        [Theory]
        [InlineData("1.0")]
        [InlineData("1.0-suffix")]
        [InlineData("1.0.1")]
        [InlineData("1.0.1-suffix")]
        [InlineData("0.0")]
        [InlineData("0.0-suffix")]
        [InlineData("0.0.1")]
        [InlineData("0.0.1-suffix")]
        [InlineData("0.10.13")]
        [InlineData("0.10.13-18")]
        [InlineData("123.456")]
        [InlineData("123.456-0d38f7")]
        [InlineData("123.456.789")]
        public void WillMatchValidVersionWithSuffix(string version)
        {
            Assert.True(version.IsVersionNumber(allowSuffix: true));
        }

        /// <summary>
        /// Invalid version numbers will not match
        /// </summary>
        [Theory]
        [InlineData("1")]
        [InlineData("01.2")]
        [InlineData("01.2.3")]
        [InlineData("1.2 3")]
        [InlineData("1.2.3-has space")]
        [InlineData("1.2.3-----")]
        [InlineData("")]
        [InlineData(null)]
        public void WillNotMatchInvalidVersionWithSuffix(string version)
        {
            Assert.False(version.IsVersionNumber(allowSuffix: true));
        }
    }
}
