﻿using Xunit;

namespace FlipSky.Strings.Test
{
    /// <summary>
    /// Verify identifier matching
    /// </summary>
    public class IdentifierTests
    {
        /// <summary>
        /// Verify that valid identifiers are matched
        /// </summary>
        [Theory]
        [InlineData("identifier")]
        [InlineData("identifier00")]
        [InlineData("ident00other")]
        [InlineData("ident_with_underscore")]
        [InlineData("ident0_with1_underscore2")]
        public void WillMatchValidIdentifier(string identifier)
        {
            Assert.True(identifier.IsIdentifier());
        }

        /// <summary>
        /// Verify that valid identifiers are matched
        /// </summary>
        [Theory]
        [InlineData("identifier")]
        [InlineData("identifier00")]
        [InlineData("ident00other")]
        [InlineData("ident_with_underscore")]
        [InlineData("ident0_with1_underscore2")]
        [InlineData("ident-with-underscore")]
        [InlineData("ident0-with1_underscore2-And-dashes")]
        public void WillMatchValidIdentifierWithDashes(string identifier)
        {
            Assert.True(identifier.IsIdentifier(allowDashes: true));
        }

        /// <summary>
        /// Verify that invalid identifiers are not matched
        /// </summary>
        [Theory]
        [InlineData(" identifier")]
        [InlineData("identifier 00")]
        [InlineData("ident-other")]
        [InlineData("_identifier")]
        [InlineData("0identifier")]
        [InlineData(null)]
        [InlineData("")]
        public void WillNotMatchInvalidIdentifier(string identifier)
        {
            Assert.False(identifier.IsIdentifier());
        }

        /// <summary>
        /// Verify that invalid identifiers are not matched
        /// </summary>
        [Theory]
        [InlineData(" identifier")]
        [InlineData("identifier 00")]
        [InlineData("_identifier")]
        [InlineData("-identifier")]
        [InlineData("0identifier")]
        [InlineData(null)]
        [InlineData("")]
        public void WillNotMatchInvalidIdentifierWithDashes(string identifier)
        {
            Assert.False(identifier.IsIdentifier());
        }
    }
}
