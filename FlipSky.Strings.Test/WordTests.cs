﻿using System.Linq;
using Xunit;

namespace FlipSky.Strings.Test
{
    public class WordTests
    {
        /// <summary>
        /// Verify that single words will correctly match
        /// </summary>
        /// <param name="value"></param>
        [Theory]
        [InlineData("word")]
        [InlineData("don't")]
        [InlineData("\"quoted\"")]
        public void WillMatchSingleWord(string value)
        {
            Assert.True(value.IsSingleWord());
        }

        /// <summary>
        /// Verify that non single words will not match
        /// </summary>
        [Theory]
        [InlineData(" word")]
        [InlineData("do not")]
        public void WillNotMatchMultipleWords(string value)
        {
            Assert.False(value.IsSingleWord());
        }

        [Theory]
        [InlineData("this is a sentence", new string[] { "this", "is", "a", "sentence" })]
        [InlineData(" leading and trailing spaces are ignored ", new string[] { "leading", "and", "trailing", "spaces", "are", "ignored" })]
        [InlineData("will split\nover lines", new string[] { "will", "split", "over", "lines" })]
        public void WillFindIndividualWords(string source, string[] expected)
        {
            var result = source.SplitIntoWords().ToArray();
            Assert.Equal(expected.Length, result.Length);
            for (int i = 0; i < expected.Length; i++)
                Assert.Equal(expected[i], result[i]);
        }
    }
}
