#!/bin/sh
#----------------------------------------------------------------------------
# Generate coverage report (Linx/OSX)
#----------------------------------------------------------------------------
dotnet test
reportgenerator -reports:coverage/*.xml -targetdir:coverage/html -reporttypes:html
reportgenerator -reports:coverage/*.xml -targetdir:coverage/badges -reporttypes:badges
