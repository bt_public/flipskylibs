﻿namespace FlipSky.Ensures
{
    using System;

    /// <summary>
    /// Various tests for parameter values.
    /// 
    /// These helpers simplify the process of checking parameters values. Each provides
    /// a generic form that can raise any specified exception as well as a version that
    /// just raises an ArgumentException (most common use case)
    /// </summary>
    public static class Ensure
    {
        /// <summary>
        /// Create and throw the exception with an optional message
        /// </summary>
        private static void ThrowException<T>(string message) where T : Exception, new()
        {
            T ex = null;
            if (message != null)
            {
                try
                {
                    ex = Activator.CreateInstance(typeof(T), message) as T;
                }
                catch (Exception)
                {
                    // Ignore and fall through
                }
            }
            if (ex == null)
                ex = new T();
            throw ex;
        }

        /// <summary>
        /// Throw an exception of type T if any of the parameters is not null
        /// </summary>
        public static void IsNull<T>(object target, string message = null) where T : Exception, new()
        {
            if (target != null)
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if any parameter is not null
        /// </summary>
        public static void IsNull(object target, string message = null)
        {
            IsNull<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the parameters is null
        /// </summary>
        public static void IsNotNull<T>(object target, string message = null) where T : Exception, new()
        {
            if (target == null)
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the parameter is null
        /// </summary>
        public static void IsNotNull(object target, string message = null)
        {
            IsNotNull<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if any of the parameters is not true
        /// </summary>
        public static void IsTrue<T>(bool target, string message = null) where T : Exception, new()
        {
            if (!target)
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if any of the parameters is not true
        /// </summary>
        public static void IsTrue(bool target, string message = null)
        {
            IsTrue<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the parameter is not false
        /// </summary>
        public static void IsFalse<T>(bool target, string message = null) where T : Exception, new()
        {
            if (target)
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the parameter is not false
        /// </summary>
        public static void IsFalse(bool target, string message = null)
        {
            IsFalse<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the string parameter is not an empty string or null
        /// </summary>
        public static void IsNullOrEmpty<T>(string target, string message = null) where T : Exception, new()
        {
            if (!string.IsNullOrEmpty(target))
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the string parameter is not an empty string or null
        /// </summary>
        public static void IsNullOrEmpty(string target, string message = null)
        {
            IsNullOrEmpty<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the string parameter is empty or null.
        /// </summary>
        public static void IsNotNullOrEmpty<T>(string target, string message = null) where T : Exception, new()
        {
            if (string.IsNullOrEmpty(target))
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the string parameter is empty or null.
        /// </summary>
        public static void IsNotNullOrEmpty(string target, string message = null)
        {
            IsNotNullOrEmpty<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the string parameter is not null or doesn't contain only white space
        /// </summary>
        public static void IsNullOrWhiteSpace<T>(string target, string message = null) where T : Exception, new()
        {
            if (!string.IsNullOrWhiteSpace(target))
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the string parameter is not null or doesn't contain only white space
        /// </summary>
        public static void IsNullOrWhiteSpace(string target, string message = null)
        {
            IsNullOrWhiteSpace<ArgumentException>(target, message);
        }

        /// <summary>
        /// Throw an exception of type T if the string parameter is null or contains only white space
        /// </summary>
        public static void IsNotNullOrWhiteSpace<T>(string target, string message = null) where T : Exception, new()
        {
            if (string.IsNullOrWhiteSpace(target))
                ThrowException<T>(message);
        }

        /// <summary>
        /// Throw an ArgumentException if the string parameter is null or contains only white space
        /// </summary>
        public static void IsNotNullOrWhiteSpace(string target, string message = null)
        {
            IsNotNullOrWhiteSpace<ArgumentException>(target, message);
        }
    }
}