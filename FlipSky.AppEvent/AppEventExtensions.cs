﻿using System;
using System.Threading;
using FlipSky.Ensures;
using FlipSky.Topics;
using FlipSky.Reflection;

namespace FlipSky.AppEvent
{
    /// <summary>
    /// Extension methods for AppEvent instances
    /// </summary>
    public static class AppEventExtensions
    {
        /// <summary>
        /// The topic used for start messages
        /// </summary>
        public const string AppEventStartTopic = "system/start";

        /// <summary>
        /// Publish using a string rather than a topic instance
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="topic"></param>
        /// <param name="payload"></param>
        public static void Publish(this IAppEvent appEvent, string topic, object payload)
        {
            appEvent.Publish(new Topic(topic), payload);
        }

        /// <summary>
        /// Subscribe using a string rather than a topic instance
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="topic"></param>
        /// <param name="handler"></param>
        public static void Subscribe(this IAppEvent appEvent, string topic, AppEventHandler handler)
        {
            appEvent.Subscribe(new Topic(topic), handler);
        }

        /// <summary>
        /// Subscribe to all decorated methods on the given instance
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="instance"></param>
        public static void Subscribe(this IAppEvent appEvent, object instance)
        {
            Ensure.IsNotNull(instance);
            var methods = ReflectionHelpers.FindMethods<HandlesAppEvent>(instance.GetType());
            // Bind each of them
            foreach (var m in methods.Keys)
            {
                // Create a delegate for the method
                var handler = Delegate.CreateDelegate(typeof(AppEventHandler), instance, m) as AppEventHandler;
                Ensure.IsNotNull(handler);
                // Bind it to all of the topics specified
                foreach (var attr in methods[m])
                    appEvent.Subscribe(attr.ConstructorArguments[0].Value.ToString(), handler);
            }
        }

        /// <summary>
        /// Subscribe to the 'start' message (always the first message fired)
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="handler"></param>
        public static void OnAppEventStart(this IAppEvent appEvent, AppEventHandler handler)
        {
            appEvent.Subscribe(AppEventStartTopic, handler);
        }

        /// <summary>
        /// Run the message loop on a background thread
        /// </summary>
        /// <param name="appEvent"></param>
        public static void Start(this IAppEvent appEvent)
        {
            var thread = new Thread(() => appEvent.Run());
            thread.IsBackground = true;
            thread.Start();
        }
    }
}
