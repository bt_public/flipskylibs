﻿using FlipSky.Topics;

namespace FlipSky.AppEvent
{
    /// <summary>
    /// Delegate for handling application events
    /// </summary>
    /// <param name="topic"></param>
    /// <param name="payload"></param>
    public delegate void AppEventHandler(Topic topic, object payload);

    /// <summary>
    /// Internal application messaging.
    /// </summary>
    public interface IAppEvent
    {
        /// <summary>
        /// Public an event on the message bus
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="payload"></param>
        void Publish(Topic topic, object payload);

        /// <summary>
        /// Subscribe to messages on the message bus
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="handler"></param>
        void Subscribe(Topic topic, AppEventHandler handler);

        /// <summary>
        /// Run the service main loop
        /// </summary>
        void Run();
    }
}
