﻿using System;

namespace FlipSky.AppEvent
{
    /// <summary>
    /// Attribute for marking methods that process app events
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class HandlesAppEvent : Attribute
    {
        /// <summary>
        /// Constructor with the topic string
        /// </summary>
        /// <param name="topic"></param>
        public HandlesAppEvent(string topic)
        {
            // Nothing to do
        }
    }
}
