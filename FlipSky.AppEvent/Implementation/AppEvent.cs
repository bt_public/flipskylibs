﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using FlipSky.Ensures;
using FlipSky.Logging;
using FlipSky.Depends;
using FlipSky.Topics;

namespace FlipSky.AppEvent.Implementation
{
    /// <summary>
    /// Default implementation of the AppEvent main loop
    /// </summary>
    [DefaultImplementation(typeof(IAppEvent), Lifetime.Singleton)]
    public class AppEvent : IAppEvent
    {
        /// <summary>
        /// The logger instance
        /// </summary>
        private readonly ILogger Log;

        /// <summary>
        /// The message queue
        /// </summary>
        private readonly ConcurrentQueue<AppEventData> m_queue;

        /// <summary>
        /// The map of event handlers
        /// </summary>
        private readonly Dictionary<Topic, HashSet<AppEventHandler>> m_handlers;

        /// <summary>
        /// The event for triggering payload processing
        /// </summary>
        private readonly AutoResetEvent m_event;

        /// <summary>
        /// Set to true if running
        /// </summary>
        private bool m_running;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        /// <param name="logger"></param>
        public AppEvent(ILogger logger)
        {
            Log = logger;
            m_queue = new ConcurrentQueue<AppEventData>();
            m_handlers = new Dictionary<Topic, HashSet<AppEventHandler>>();
            m_event = new AutoResetEvent(false);
            m_running = false;
            // Make sure the first message published is the 'start' event
            Publish(new Topic(AppEventExtensions.AppEventStartTopic), null);
        }

        #region Public API
        /// <summary>
        /// Publish an event to the given topic
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="payload"></param>
        public void Publish(Topic topic, object payload)
        {
            // Verify parameters
            Ensure.IsNotNull(topic);
            Ensure.IsFalse(topic.ContainsWildcards);
            // Add the entry to the queue
            lock (m_queue)
            {
                m_queue.Enqueue(new AppEventData() { Topic = topic, Payload = payload });
                if (m_running)
                    m_event.Set();
            }
        }

        /// <summary>
        /// Subscribe to a given topic
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="handler"></param>
        public void Subscribe(Topic topic, AppEventHandler handler)
        {
            // Verify parameters
            Ensure.IsNotNull(topic);
            Ensure.IsNotNull(handler);
            // Add the handler
            lock (m_handlers)
            {
                if (!m_handlers.ContainsKey(topic))
                    m_handlers[topic] = new HashSet<AppEventHandler>();
                m_handlers[topic].Add(handler);
            }
        }

        /// <summary>
        /// Run the main event loop
        /// </summary>
        public void Run()
        {
            Ensure.IsFalse(m_running);
            m_running = true;
            while (true)
            {
                // Process all pending message
                while (m_queue.TryDequeue(out var appEventData))
                {
                    // Find out who is interested in the event
                    var handlers = new HashSet<AppEventHandler>();
                    lock (m_handlers)
                    {
                        // Add all handlers that match the subscription
                        foreach (var subscription in m_handlers.Keys)
                        {
                            if (subscription.Matches(appEventData.Topic))
                                handlers.UnionWith(m_handlers[subscription]);
                        }
                    }
                    // Dispatch it to all handlers
                    Log.Info("Dispatching AppEvent on topic '{0}' to {1} handlers.", appEventData.Topic.ToString(), handlers.Count);
                    foreach (var handler in handlers)
                    {
                        try
                        {
                            handler.Invoke(appEventData.Topic, appEventData.Payload);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "Unhandled exception while dispatching message on topic '{0}'", appEventData.Topic.ToString());
                        }
                    }
                }
                // Wait for the next event to come in
                try
                {
                    m_event.WaitOne(1000);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Exception while waiting for the next event.");
                }
            }
        }

        #endregion
    }
}
