﻿using FlipSky.Topics;

namespace FlipSky.AppEvent.Implementation
{
    /// <summary>
    /// Used to hold queued app events
    /// </summary>
    internal class AppEventData
    {
        /// <summary>
        /// The topic the data was published on
        /// </summary>
        public Topic Topic;

        /// <summary>
        /// The payload for the message
        /// </summary>
        public object Payload;
    }
}
