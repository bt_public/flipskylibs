﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyModel;
using FlipSky.Ensures;

namespace FlipSky.Reflection
{
    /// <summary>
    /// Helper methods to perform reflection related tasks
    /// </summary>
    public static class ReflectionHelpers
    {
        #region Helpers
        /// <summary>
        /// Check the type parameter and return the associated TypeInfo structure
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static TypeInfo SafelyGetTypeInfo(Type t)
        {
            Ensure.IsNotNull(t);
            return t.GetTypeInfo();
        }
        #endregion

        /// <summary>
        /// Final all loaded assemblies (at the time of the call)
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> FindAssemblies()
        {
            var assemblies = new List<Assembly>();
            foreach (var library in DependencyContext.Default.RuntimeLibraries)
            {
                try
                {
                    assemblies.Add(Assembly.Load(new AssemblyName(library.Name)));
                }
                catch (FileNotFoundException)
                {
                    // Ignore this (injected assemblies cannot be loaded)
                }
            }
            return assemblies;
        }

        /// <summary>
        /// Given a list of assemblies return the set that reference a specified one
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="fromSelection"></param>
        /// <returns></returns>
        public static IEnumerable<Assembly> FindAssembliesThatReference(Assembly reference, IEnumerable<Assembly> fromSelection = null)
        {
            // Check parameters
            Ensure.IsNotNull(reference);
            if (fromSelection == null)
                fromSelection = FindAssemblies();
            // Now build up our result set
            var referenceName = reference.GetName().Name;
            return fromSelection.Where(a => a.GetReferencedAssemblies().Select(r => r.Name).Contains(referenceName));
        }

        /// <summary>
        /// Find all concrete classes in one or more assemblies
        /// </summary>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static IEnumerable<Type> FindClasses(IEnumerable<Assembly> assemblies)
        {
            // If no assemblies provided search everything
            if ((assemblies == null) || (assemblies.Count() == 0))
                assemblies = FindAssemblies();
            // Find all non-abstract classes
            var results = new List<Type>();
            foreach (var assembly in assemblies)
            {
                results.AddRange(assembly.DefinedTypes.Where(
                    t =>
                    {
                        var info = SafelyGetTypeInfo(t);
                        return info.IsClass && !info.IsAbstract;
                    }));
            }
            return results;
        }

        /// <summary>
        /// Find all concrete classes in one or more assemblies
        /// </summary>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static IEnumerable<Type> FindClasses(params Assembly[] assemblies)
        {
            return FindClasses((IEnumerable<Assembly>)assemblies);
        }

        /// <summary>
        /// Find all classes in the specific assemblies that have the given attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static Dictionary<Type, List<CustomAttributeData>> FindClasses<T>(IEnumerable<Assembly> assemblies) where T : Attribute
        {
            var results = new Dictionary<Type, List<CustomAttributeData>>();
            foreach (var candidate in FindClasses(assemblies).Where(t => t.CustomAttributes.Where(c => c.AttributeType == typeof(T)).Any()))
                results[candidate] = candidate.CustomAttributes.Where(c => c.AttributeType == typeof(T)).ToList();
            return results;
        }

        /// <summary>
        /// Find all classes in the specific assemblies that have the given attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static Dictionary<Type, List<CustomAttributeData>> FindClasses<T>(params Assembly[] assemblies) where T : Attribute
        {
            return FindClasses<T>((IEnumerable<Assembly>)assemblies);
        }

        /// <summary>
        /// Find all methods on a class that are decorated with the specified attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Dictionary<MethodInfo, List<CustomAttributeData>> FindMethods<T>(Type t) where T : Attribute
        {
            var info = SafelyGetTypeInfo(t);
            Ensure.IsTrue(info.IsClass && !info.IsAbstract);
            // Look at all the methods and select those with the requested attribute
            var results = new Dictionary<MethodInfo, List<CustomAttributeData>>();
            foreach (var method in info.GetMethods())
            {
                var attributes = method.CustomAttributes.Where(a => a.AttributeType == typeof(T));
                if (attributes.Any())
                    results[method] = attributes.ToList();
            }
            return results;
        }
    }
}
