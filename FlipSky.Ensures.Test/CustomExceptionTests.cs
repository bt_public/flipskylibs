﻿using System;
using Xunit;

namespace FlipSky.Ensures.Test
{
    public class CustomExceptionTests
    {
        //--- Used to verify the message is included
        private const string ExampleMessage = "This is a custom message";

        [Fact]
        public void IsNotNullTest()
        {
            var notNull = new Object();
            try
            {
                Ensure.IsNotNull<CustomException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullTest()
        {
            var notNull = new Object();
            try
            {
                Ensure.IsNull<CustomException>(notNull, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsTrueTest()
        {
            try
            {
                Ensure.IsTrue<CustomException>(false, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsFalseTest()
        {
            try
            {
                Ensure.IsFalse<CustomException>(true, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullOrEmptyTest()
        {
            string white = "   ";
            try
            {
                Ensure.IsNullOrEmpty<CustomException>(white, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNotNullOrEmptyTest()
        {
            try
            {
                Ensure.IsNotNullOrEmpty<CustomException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullOrWhiteSpaceTest()
        {
            const string NotNullOrWhiteSpace = "notnullorwhitespace";
            try
            {
                Ensure.IsNullOrWhiteSpace<CustomException>(NotNullOrWhiteSpace, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNotNullOrWhiteSpaceTest()
        {
            try
            {
                Ensure.IsNotNullOrWhiteSpace<CustomException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (CustomException ex)
            {
                Assert.Equal(CustomException.EnforcedMessage, ex.Message);
            }
        }
    }
}