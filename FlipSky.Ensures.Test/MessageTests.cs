﻿using System;
using Xunit;

namespace FlipSky.Ensures.Test
{
    public class MessageTests
    {
        //--- Used to verify the message is included
        private const string ExampleMessage = "This is a custom message";

        [Fact]
        public void IsNotNullTest()
        {
            var notNull = new Object();
            try
            {
                Ensure.IsNotNull(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNotNull<InvalidOperationException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullTest()
        {
            var notNull = new Object();
            try
            {
                Ensure.IsNull(notNull, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNull<InvalidOperationException>(notNull, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsTrueTest()
        {
            try
            {
                Ensure.IsTrue(false, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsTrue<InvalidOperationException>(false, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsFalseTest()
        {
            try
            {
                Ensure.IsFalse(true, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsFalse<InvalidOperationException>(true, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullOrEmptyTest()
        {
            string white = "   ";
            try
            {
                Ensure.IsNullOrEmpty(white, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNullOrEmpty<InvalidOperationException>(white, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNotNullOrEmptyTest()
        {
            try
            {
                Ensure.IsNotNullOrEmpty(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNotNullOrEmpty<InvalidOperationException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNullOrWhiteSpaceTest()
        {
            const string NotNullOrWhiteSpace = "notnullorwhitespace";
            try
            {
                Ensure.IsNullOrWhiteSpace(NotNullOrWhiteSpace, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNullOrWhiteSpace<InvalidOperationException>(NotNullOrWhiteSpace, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }

        [Fact]
        public void IsNotNullOrWhiteSpaceTest()
        {
            try
            {
                Ensure.IsNotNullOrWhiteSpace(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (ArgumentException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
            try
            {
                Ensure.IsNotNullOrWhiteSpace<InvalidOperationException>(null, ExampleMessage);
                Assert.True(false); // Did not throw an exception
            }
            catch (InvalidOperationException ex)
            {
                Assert.Equal(ExampleMessage, ex.Message);
            }
        }
    }
}