﻿using System;

namespace FlipSky.Ensures.Test
{
    internal class CustomException : Exception
    {
        //--- Always use this message
        public const string EnforcedMessage = "This message cannot be changed";

        /// <summary>
        /// No constructor that takes a message, just a default
        /// </summary>
        public CustomException() : base(EnforcedMessage) { }
    }
}
