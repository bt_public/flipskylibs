﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using FlipSky.Ensures;

namespace FlipSky.Resource
{
    /// <summary>
    /// Provide access to resources in an assembly as a readonly file system
    /// </summary>
    public class ResourceFiles
    {
        /// <summary>
        /// The assembly containing the files
        /// </summary>
        private readonly Assembly Assembly;

        /// <summary>
        /// The base path for the resource files
        /// </summary>
        private readonly string BasePath;

        /// <summary>
        /// The full list of resources in the assembly
        /// </summary>
        private readonly List<string> Resources;

        /// <summary>
        /// Constructor with an assembly and an optional subpath
        /// </summary>
        public ResourceFiles(Assembly assembly, string basePath = null)
        {
            // Verify parameters
            Ensure.IsNotNull<ArgumentException>(assembly);
            if (basePath != null)
                Ensure.IsNotNullOrWhiteSpace<ArgumentException>(basePath);
            // Build the path
            Assembly = assembly;
            var path = Assembly.GetName().Name;
            if (basePath != null)
                path = $"{path}.{ConvertPath(basePath)}";
            BasePath = path;
            // Get the list of resources
            Resources = new List<string>(Assembly.GetManifestResourceNames());
        }

        /// <summary>
        /// Constructor using a class type. Resources are assumed
        /// to be relative to the classes location in the namespace.
        /// </summary>
        public ResourceFiles(Type forClass, string basePath = null)
        {
            Ensure.IsNotNull<ArgumentException>(forClass);
            Assembly = forClass.Assembly;
            // Calculate the path
            var parts = forClass.FullName.Split('.');
            var path = string.Join(".", parts.Take(parts.Length - 1));
            if (basePath != null)
                path = $"{path}.{ConvertPath(basePath)}";
            BasePath = path;
            // Get the list of resources
            Resources = new List<string>(Assembly.GetManifestResourceNames());
        }

        #region Helpers
        /// <summary>
        /// Convert path separates ('/' and '\') into dots for resource
        /// path compatibility
        /// </summary>
        private string ConvertPath(string path)
        {
            return path.Replace('/', '.').Replace('\\', '.');
        }

        /// <summary>
        /// Get the full path to the target resource
        /// </summary>
        private string GetFullPath(string path)
        {
            if (path == null)
                return BasePath;
            return $"{BasePath}.{ConvertPath(path)}";
        }
        #endregion

        #region Public API
        /// <summary>
        /// Determine if the named resource exists
        /// </summary>
        public bool Exists(string path)
        {
            return IsFile(path) || IsDir(path);
        }

        /// <summary>
        /// Determine if the named resource exists and is a file
        /// </summary>
        public bool IsFile(string path)
        {
            Ensure.IsNotNullOrWhiteSpace(path);
            return Resources.Contains(GetFullPath(path));
        }

        /// <summary>
        /// Determine if the named resource exists and is a directory
        /// We assume filenames consist of a name and an extension
        /// </summary>
        public bool IsDir(string path)
        {
            Ensure.IsNotNullOrWhiteSpace(path);
            var full = GetFullPath(path);
            // If it is a file it can't be a directory
            if (Resources.Contains(full))
                return false;
            // If this is a prefix for anything that contains only
            // a single '.' it is a directory
            var prefix = full + ".";
            return Resources
                .Where(r =>
                    r.StartsWith(prefix) &&
                    r.Substring(prefix.Length).Contains('.')
                    )
                .Any();
        }

        /// <summary>
        /// List the files contained in the named directory
        /// </summary>
        public IEnumerable<string> ListFiles(string path = null)
        {
            if (path != null)
                Ensure.IsTrue<DirectoryNotFoundException>(IsDir(path));
            // Return anything that starts with this and contains
            // a single '.'
            var prefix = GetFullPath(path) + ".";
            return Resources
                .Where(r =>
                    r.StartsWith(prefix) &&
                    r.Substring(prefix.Length).Where(d => d == '.').Count() == 1
                )
                .Select(r =>
                    r.Substring(prefix.Length)
                );
        }

        /// <summary>
        /// List the directories contained in the named directory
        /// </summary>
        public IEnumerable<string> ListDirs(string path = null)
        {
            if (path != null)
                Ensure.IsTrue<DirectoryNotFoundException>(IsDir(path));
            // Select all child components that have a single trailing component
            var prefix = GetFullPath(path) + ".";
            var components = prefix.Split('.').Length + 2; // Need two additional components
            return Resources
                .Where(r =>
                    r.StartsWith(prefix) &&
                    r.Split('.').Length == components
                    )
                .Select(r =>
                    r.Substring(prefix.Length).Split('.')[0]
                    )
                .Distinct();
        }

        /// <summary>
        /// Open the resource file as a stream
        /// </summary>
        public Stream Open(string path)
        {
            Ensure.IsTrue<FileNotFoundException>(IsFile(path));
            return Assembly.GetManifestResourceStream(GetFullPath(path));
        }

        /// <summary>
        /// Read the entire resource as a string
        /// </summary>
        public string ReadAsString(string path)
        {
            using (var s = Open(path))
            using (var r = new StreamReader(s))
                return r.ReadToEnd();
        }
        #endregion
    }
}
