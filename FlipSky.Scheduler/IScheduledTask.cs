﻿namespace FlipSky.Scheduler
{
    /// <summary>
    /// Represents a scheduled task registered with the scheduler
    /// 
    /// TODO: The scheduler should be moved to separate NuGet package in future
    /// </summary>
    public interface IScheduledTask
    {
        /// <summary>
        /// Indiciate if the task is still active
        /// </summary>
        bool Active { get; }

        /// <summary>
        /// Remove this task from the schedule
        /// </summary>
        void Remove();
    }
}
