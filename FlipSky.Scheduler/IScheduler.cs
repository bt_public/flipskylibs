﻿using System;
using System.Threading.Tasks;

namespace FlipSky.Scheduler
{
    /// <summary>
    /// Interface to a scheduling service to allow background operations
    /// to be performed on time periods.
    /// 
    /// The implementation of this interface is assumed to be a singleton
    /// </summary>
    public interface IScheduler
    {
        /// <summary>
        /// Schedule a task to run after a specified delay. Once
        /// the task has executed the schedule will be removed.
        /// </summary>
        [Obsolete("Please use OneShot(int, Action) instead")]
        IScheduledTask OneShot(int delayInSeconds, Func<Task> action);

        /// <summary>
        /// Schedule a task to run after a specified delay. Once
        /// the task has executed the schedule will be removed.
        /// </summary>
        IScheduledTask OneShot(int delayInSeconds, Action action);

        /// <summary>
        /// Schedule a task to run on a periodic basis. The task will
        /// be run after the intial 'delayInSeconds' has expired and
        /// then every 'periodInSeconds' after that.
        /// </summary>
        [Obsolete("Please use Periodic(int, int, Action) instead")]
        IScheduledTask Periodic(int delayInSeconds, int periodInSeconds, Func<Task> action);

        /// <summary>
        /// Schedule a task to run on a periodic basis. The task will
        /// be run after the intial 'delayInSeconds' has expired and
        /// then every 'periodInSeconds' after that.
        /// </summary>
        IScheduledTask Periodic(int delayInSeconds, int periodInSeconds, Action action);

        /// <summary>
        /// Use a 'cron' format schedule string to specify when the task should
        /// run. This method uses specific dates and times in the local timezone.
        /// 
        /// The schedule format is:
        /// 
        /// min hour dom month dow
        /// 
        /// Each entry may be '*' to match all valid values for the field,
        /// a list of valid values separated by commas or a single valid
        /// value.
        /// 
        /// Examples:
        /// 
        ///  "30 18 * * *" - run at 6:30PM every day
        /// 
        /// http://www.adminschoice.com/crontab-quick-reference
        /// </summary>
        [Obsolete("Please use Schedule(string, Action) instead")]
        IScheduledTask Schedule(string schedule, Func<Task> action);

        /// <summary>
        /// Use a 'cron' format schedule string to specify when the task should
        /// run. This method uses specific dates and times in the local timezone.
        /// 
        /// The schedule format is:
        /// 
        /// min hour dom month dow
        /// 
        /// Each entry may be '*' to match all valid values for the field,
        /// a list of valid values separated by commas or a single valid
        /// value.
        /// 
        /// Examples:
        /// 
        ///  "30 18 * * *" - run at 6:30PM every day
        /// 
        /// http://www.adminschoice.com/crontab-quick-reference
        /// </summary>
        IScheduledTask Schedule(string schedule, Action action);
    }
}
