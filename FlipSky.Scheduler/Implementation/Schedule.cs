﻿using System;
using System.Linq;
using System.Collections.Generic;
using FlipSky.Ensures;

namespace FlipSky.Scheduler.Implementation
{
    /// <summary>
    /// Represents a repeating schedule. This mimics the 'cron' format
    /// for schedule specification.
    /// 
    /// http://www.adminschoice.com/crontab-quick-reference
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// Days of week to match
        /// </summary>
        public IReadOnlyList<int> DayOfWeek { get; private set; }

        /// <summary>
        /// Month of year
        /// </summary>
        public IReadOnlyList<int> Month { get; private set; }

        /// <summary>
        /// Day of month (1 - 31)
        /// </summary>
        public IReadOnlyList<int> DayOfMonth { get; private set; }

        /// <summary>
        /// Hour of day (0 - 23)
        /// </summary>
        public IReadOnlyList<int> Hour { get; private set; }

        /// <summary>
        /// Minute of hour (0 - 59)
        /// </summary>
        public IReadOnlyList<int> Minute { get; private set; }

        /// <summary>
        /// Constructor from a string version of the schedule
        /// </summary>
        /// <param name="schedule"></param>
        public Schedule(string schedule)
        {
            // Split into the 5 parts
            var parts = schedule.Split(' ');
            Ensure.IsTrue<ArgumentException>(parts.Length == 5);
            // Now process each part
            Minute = SplitComponent(parts[0], 0, 59);
            Hour = SplitComponent(parts[1], 0, 23);
            DayOfMonth = SplitComponent(parts[2], 1, 31);
            Month = SplitComponent(parts[3], 1, 12);
            DayOfWeek = SplitComponent(parts[4], 0, 6);
        }

        /// <summary>
        /// Determine if the given DateTime is triggered by this
        /// schedule
        /// </summary>
        /// <param name="when"></param>
        /// <returns></returns>
        public bool IsTriggered(DateTime when)
        {
            return
                ((Minute == null) || Minute.Contains(when.Minute)) &&
                ((Hour == null) || Hour.Contains(when.Hour)) &&
                ((DayOfMonth == null) || DayOfMonth.Contains(when.Day)) &&
                ((Month == null) || Month.Contains(when.Month)) &&
                ((DayOfWeek == null) || DayOfWeek.Contains((int)when.DayOfWeek));
        }

        /// <summary>
        /// Parse a comma separate list of values. Any errors (invalid format,
        /// value out of range) will result in an exception.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="minVal"></param>
        /// <param name="maxVal"></param>
        /// <returns></returns>
        private List<int> SplitComponent(string source, int minVal, int maxVal)
        {
            // Clean up input string and check for wildcard
            source = source.Trim();
            if (source == "*")
                return null;
            // Process each entry
            var parts = source.Split(',');
            var result = new List<int>();
            foreach (var allowed in parts)
            {
                var value = int.Parse(allowed);
                Ensure.IsTrue((value >= minVal) && (value <= maxVal));
                result.Add(value);
            }
            // All done
            return result;
        }
    }
}
