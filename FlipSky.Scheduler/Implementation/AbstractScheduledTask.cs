﻿using System;
using System.Threading.Tasks;
using FlipSky.Logging;

namespace FlipSky.Scheduler.Implementation
{
    /// <summary>
    /// Base class for scheduled task
    /// </summary>
    internal abstract class AbstractScheduledTask : IScheduledTask
    {
        /// <summary>
        /// Reference to the containing scheduler
        /// </summary>
        private readonly Scheduler Scheduler;

        /// <summary>
        /// The action to run
        /// </summary>
        private readonly Action Action;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scheduler"></param>
        protected AbstractScheduledTask(Scheduler scheduler, Action action)
        {
            Scheduler = scheduler;
            Action = action;
            Active = true;
            Running = false;
        }

        #region Private API
        /// <summary>
        /// Return the amount of time (in seconds) until the next
        /// scheduled invocation of this task.
        /// </summary>
        public abstract int TimeToNextInvocation { get; }

        /// <summary>
        /// Return true if the task is ready to run now
        /// </summary>
        public abstract bool ScheduledToRun { get; }

        /// <summary>
        /// Return true if the task is currently running
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// This method is called after the task finishes
        /// </summary>
        protected virtual void AfterInvoke()
        {
            // Do nothing here
        }

        /// <summary>
        /// Execute the task. This starts the task in an async
        /// manner.
        /// </summary>
        public void Invoke(int taskId)
        {
            try
            {
                Action();
            }
            catch (Exception ex)
            {
                Scheduler.Log.Error(ex, "Unhandled exception executing task #{0}", taskId);
            }
            finally
            {
                AfterInvoke();
                Running = false;
            }
        }

        /// <summary>
        /// Mark the task as running (or queued to be run)
        /// </summary>
        public void SetRunning()
        {
            Running = true;
        }
        #endregion

        #region Public API
        /// <summary>
        /// Determine if the schedule is active or not
        /// </summary>
        public bool Active { get; private set; }

        /// <summary>
        /// Remove the task from the schedule
        /// </summary>
        public void Remove()
        {
            if (!Active)
                return;
            Active = false;
            Scheduler.RemoveScheduledTask(this);
        }
        #endregion
    }
}
