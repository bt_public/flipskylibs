﻿using System;
using System.Threading.Tasks;

namespace FlipSky.Scheduler.Implementation
{
    /// <summary>
    /// Schedule using a 'cron' style specification
    /// </summary>
    internal class CronTask : AbstractScheduledTask
    {
        /// <summary>
        /// The schedule to apply to this task
        /// </summary>
        private readonly Schedule m_schedule;

        /// <summary>
        /// When the schedule was last run
        /// </summary>
        private int m_lastRun;

        /// <summary>
        /// Determine when to next run.
        /// 
        /// In this case it's hard to predict but it will always be on a minute
        /// boundary so just return the time until then.
        /// </summary>
        public override int TimeToNextInvocation => GetRemainingSeconds();

        /// <summary>
        /// Determine if we are scheduled to run now
        /// </summary>
        public override bool ScheduledToRun => (GetMinuteInYear() > m_lastRun) && m_schedule.IsTriggered(DateTime.Now);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scheduler"></param>
        /// <param name="schedule"></param>
        /// <param name="action"></param>
        public CronTask(Scheduler scheduler, string schedule, Action action) : base(scheduler, action)
        {
            m_schedule = new Schedule(schedule);
            m_lastRun = 0;
        }

        /// <summary>
        /// Convert a DateTime into minutes since epoch 
        /// </summary>
        /// <returns></returns>
        private int GetMinuteInYear()
        {
            var now = DateTime.Now;
            var epoch = new DateTime(now.Year, 1, 1);
            return (int)(now - epoch).TotalMinutes;
        }
  
        /// <summary>
        /// Get the number of seconds remaining until the next minute
        /// boundary
        /// </summary>
        /// <returns></returns>
        private int GetRemainingSeconds()
        {
            return 60 - DateTime.Now.Second;
        }

        /// <summary>
        /// Save our last run time so we aren't triggered multiple times
        /// for the same schedule.
        /// </summary>
        protected override void AfterInvoke()
        {
            m_lastRun = GetMinuteInYear();
        }
    }
}
