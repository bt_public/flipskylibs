﻿using System;
using System.Threading.Tasks;

namespace FlipSky.Scheduler.Implementation
{
    internal class OneshotTask : AbstractScheduledTask
    {
        /// <summary>
        /// When the task should be executed
        /// </summary>
        private readonly DateTime m_executionTime;

        /// <summary>
        /// How long until we should run
        /// </summary>
        public override int TimeToNextInvocation
        {
            get
            {
                var delay = (int)(m_executionTime - DateTime.UtcNow).TotalSeconds;
                return (delay < 0) ? 0 : delay;
            }
        }

        /// <summary>
        /// Are we ready to run?
        /// </summary>
        public override bool ScheduledToRun => TimeToNextInvocation == 0;

        /// <summary>
        /// Remove ourselves from the schedule
        /// </summary>
        protected override void AfterInvoke()
        {
            Remove();
        }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="scheduler"></param>
        /// <param name="action"></param>
        public OneshotTask(Scheduler scheduler, int delayInSeconds, Action action) : base(scheduler, action)
        {
            m_executionTime = DateTime.UtcNow.AddSeconds(delayInSeconds);
        }

    }
}
