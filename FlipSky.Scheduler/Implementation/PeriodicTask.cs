﻿using System;
using System.Threading.Tasks;

namespace FlipSky.Scheduler.Implementation
{
    internal class PeriodicTask : AbstractScheduledTask
    {
        /// <summary>
        /// Time to delay (in seconds) before the first execution
        /// </summary>
        private readonly int m_delayInSeconds;

        /// <summary>
        /// Time to delay (in seconds) between periodic invokations
        /// </summary>
        private readonly int m_periodInSeconds;

        /// <summary>
        /// When the task should next be executed
        /// </summary>
        private DateTime m_executionTime;

        /// <summary>
        /// How long until we should run
        /// </summary>
        public override int TimeToNextInvocation
        {
            get
            {
                var delay = (int)(m_executionTime - DateTime.UtcNow).TotalSeconds;
                return (delay < 0) ? 0 : delay;
            }
        }

        /// <summary>
        /// Are we ready to run?
        /// </summary>
        public override bool ScheduledToRun => TimeToNextInvocation == 0;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scheduler"></param>
        /// <param name="action"></param>
        public PeriodicTask(Scheduler scheduler, int delayInSeconds, int periodInSeconds, Action action) : base(scheduler, action)
        {
            m_delayInSeconds = delayInSeconds;
            m_periodInSeconds = periodInSeconds;
            m_executionTime = DateTime.UtcNow.AddSeconds(delayInSeconds);
        }

        /// <summary>
        /// Calculate the next execution time
        /// </summary>
        protected override void AfterInvoke()
        {
            m_executionTime = DateTime.UtcNow.AddSeconds(m_periodInSeconds);
        }
    }
}
