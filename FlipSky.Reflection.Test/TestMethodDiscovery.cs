﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlipSky.Reflection.Test.Samples;
using Xunit;

namespace FlipSky.Reflection.Test
{
    /// <summary>
    /// Test discovery of decorated methods
    /// </summary>
    public class TestMethodDiscovery
    {
        /// <summary>
        /// All methods decorated with Attribute1 will be discovered regardless of
        /// other attributes
        /// </summary>
        [Fact]
        public void WillFindMethodsWithAttribute1()
        {
            var results = ReflectionHelpers.FindMethods<MethodTestAttribute1>(typeof(DecoratedMethods));
            var names = results.Keys.Select(t => t.Name);
            Assert.Equal(3, names.Count());
            Assert.Contains("DecoratedWithAttribute1", names);
            Assert.Contains("DecoratedWithAttribute1AndAttribute2", names);
            Assert.Contains("DecoratedWithMultipleAttribute1", names);
            // Make sure that only the requested attribute is in the result set
            foreach (var attr in results.Values.SelectMany(t => t))
                Assert.Equal(typeof(MethodTestAttribute1), attr.AttributeType);
        }

        /// <summary>
        /// All methods decorated with Attribute1 will be discovered regardless of
        /// other attributes
        /// </summary>
        [Fact]
        public void WillFindMethodsWithAttribute2()
        {
            var results = ReflectionHelpers.FindMethods<MethodTestAttribute2>(typeof(DecoratedMethods));
            var names = results.Keys.Select(t => t.Name);
            Assert.Equal(2, names.Count());
            Assert.Contains("DecoratedWithAttribute2", names);
            Assert.Contains("DecoratedWithAttribute1AndAttribute2", names);
            // Make sure that only the requested attribute is in the result set
            foreach (var attr in results.Values.SelectMany(t => t))
                Assert.Equal(typeof(MethodTestAttribute2), attr.AttributeType);
        }

        [Fact]
        public void WillFindMethodsWithMultipleAttributes()
        {
            var results = ReflectionHelpers.FindMethods<MethodTestAttribute1>(typeof(DecoratedMethods));
            var method = results.Keys.Where(t => t.Name == "DecoratedWithMultipleAttribute1").First();
            var attributes = results[method];
            // Make sure both attributes are present
            Assert.Equal(2, attributes.Count());
        }
    }
}
