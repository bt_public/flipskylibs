﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.Reflection.Test.Samples
{
    /// <summary>
    /// A class with a mix of decorated methods
    /// </summary>
    internal class DecoratedMethods
    {
        public void UndecoratedMethod() { }

        [MethodTestAttribute1("one")]
        public void DecoratedWithAttribute1() { }

        [MethodTestAttribute2]
        public void DecoratedWithAttribute2() { }

        [MethodTestAttribute1("one")]
        [MethodTestAttribute2]
        public void DecoratedWithAttribute1AndAttribute2() { }

        [MethodTestAttribute1("one")]
        [MethodTestAttribute1("two")]
        public void DecoratedWithMultipleAttribute1() { }
    }
}
