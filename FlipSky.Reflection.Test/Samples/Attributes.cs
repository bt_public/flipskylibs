﻿using System;

namespace FlipSky.Reflection.Test.Samples
{
    #region Class Attributes
    /// <summary>
    /// Attribute class used for testing
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class ClassTestAttribute1 : Attribute { }

    /// <summary>
    /// Attribute class used for testing
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class ClassTestAttribute2 : Attribute { }

    /// <summary>
    /// Attribute class used for testing
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class ClassTestAttribute3 : Attribute { }
    #endregion

    #region Method Attributes
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    internal class MethodTestAttribute1 : Attribute
    {
        public MethodTestAttribute1(string parameter) { }
    }

    [AttributeUsage(AttributeTargets.Method)]
    internal class MethodTestAttribute2 : Attribute { }
    #endregion
}
