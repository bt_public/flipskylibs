﻿using System;

namespace FlipSky.Reflection.Test.Samples
{
    /// <summary>
    /// Define an interface for detection
    /// </summary>
    internal interface TestInterface
    {
        void MyMethod(string myParameter);
    }

    /// <summary>
    /// An abstract class based on the interface
    /// </summary>
    internal abstract class AbstractTestInterface : TestInterface
    {
        public abstract void MyMethod(string myParameter);
    }

    /// <summary>
    /// A concrete class based on the interface
    /// </summary>
    internal class ConcreteTestInterface : TestInterface
    {
        public void MyMethod(string myParameter)
        {
            throw new NotImplementedException();
        }
    }
}
