﻿using System;

namespace FlipSky.Reflection.Test.Samples
{
    // Test classes for discovery
    [ClassTestAttribute1]
    internal class ClassWithAttribute1 { }

    [ClassTestAttribute2]
    internal class ClassWithAttribute2 { }

    [ClassTestAttribute1]
    [ClassTestAttribute2]
    internal class ClassWithAttribute1AndAttribute2 { }
}
