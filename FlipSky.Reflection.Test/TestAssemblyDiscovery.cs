﻿using System;
using System.Linq;
using System.Reflection;
using Xunit;

namespace FlipSky.Reflection.Test
{
    /// <summary>
    /// Test discovery of loaded assemblies
    /// </summary>
    public class TestAssemblyDiscovery
    {
        /// <summary>
        /// A (partial) list of known assemblies that we reference
        /// </summary>
        private static readonly string[] KNOWN_REFERENCES = 
        {
            "FlipSky.Reflection",      // The assembly being tested
            "FlipSky.Reflection.Test", // This assembly
            "FlipSky.Ensures",         // Referenced by FlipSky.Reflection but not directly by us
            "xunit.core"               // An external package
        };

        /// <summary>
        /// We can't be sure of the full list but we can check that it
        /// includes the ones we do know we explicitly reference.
        /// </summary>
        [Fact]
        public void WillIncludeKnownAssemblies()
        {
            var names = ReflectionHelpers.FindAssemblies().Select(a => a.GetName().Name);
            foreach (var name in KNOWN_REFERENCES)
                Assert.Contains(names, n => n == name);
        }

        /// <summary>
        /// GetReferencedAssemblies will only return the set that directly
        /// reference the specific assembly.
        /// </summary>
        [Fact]
        public void WillIncludeDirectDependencies()
        {
            // FlipSky.Reflection should be the only one that references FlipSky.Ensures
            var reference = Assembly.Load("FlipSky.Ensures");
            var referencedBy = ReflectionHelpers.FindAssembliesThatReference(reference).ToList();
            Assert.Single(referencedBy);
            Assert.Equal(typeof(ReflectionHelpers).Assembly.GetName().Name, referencedBy[0].GetName().Name);
        }

        /// <summary>
        /// If no match is found the method will return an empty result set
        /// </summary>
        [Fact]
        public void CanReturnEmptyResults()
        {
            // FlipSky.Reflection should be the only one that references FlipSky.Ensures
            var reference1 = Assembly.Load("FlipSky.Ensures");
            var referencedBy = ReflectionHelpers.FindAssembliesThatReference(reference1).ToList();
            Assert.Single(referencedBy);
            Assert.Equal(typeof(ReflectionHelpers).Assembly.GetName().Name, referencedBy[0].GetName().Name);
            // FlipSky.Reflection does not reference xunit so the results should be empty
            var reference2 = typeof(Assert).Assembly;
            referencedBy = ReflectionHelpers.FindAssembliesThatReference(reference2, referencedBy).ToList();
            Assert.Empty(referencedBy);
        }
    }
}
