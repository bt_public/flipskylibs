﻿using System;
using System.Linq;
using System.Reflection;
using Xunit;
using FlipSky.Reflection.Test.Samples;

namespace FlipSky.Reflection.Test
{
    /// <summary>
    /// Test the reflection helper methods.
    /// </summary>
    public class TestClassDiscovery
    {
        /// <summary>
        /// Get the current assembly (used for testing). In this case use the assembly
        /// that contains this test class.
        /// </summary>
        /// <returns></returns>
        private Assembly GetCurrentAssembly()
        {
            return GetType().Assembly;
        }

        /// <summary>
        /// GetClasses will find all concrete classes
        /// </summary>
        [Fact]
        public void WillFindConcreteClasses()
        {
            var results = ReflectionHelpers.FindClasses(GetCurrentAssembly());
            Assert.Contains(typeof(ConcreteTestInterface), results);
        }

        /// <summary>
        /// GetClasses will not find abstract classes or interfaces
        /// </summary>
        [Fact]
        public void WillNotFindAbstractClassesOrInterfaces()
        {
            var results = ReflectionHelpers.FindClasses(GetCurrentAssembly());
            Assert.DoesNotContain(typeof(AbstractTestInterface), results);
            Assert.DoesNotContain(typeof(TestInterface), results);
        }

        /// <summary>
        /// If a specific assembly (or list of assemblies) is specified all
        /// loaded assemblies will be searched.
        /// 
        /// To test this we simply make sure a call with no parameters
        /// returns more results than one with a specific parameter.
        /// </summary>
        [Fact]
        public void IfNoAssemblySpecifiedAllAssembliesWillBeSearched()
        {
            var baseResults = ReflectionHelpers.FindClasses(GetCurrentAssembly());
            Assert.Contains(typeof(ConcreteTestInterface), baseResults);
            // Do a full scan and compare
            var results = ReflectionHelpers.FindClasses();
            Assert.Contains(typeof(ConcreteTestInterface), results);
            Assert.True(results.Count() > baseResults.Count());
        }

        /// <summary>
        /// Make sure we can find all classes with TestAttribute1
        /// </summary>
        [Fact]
        public void WillFindAllClassesWithAttribute1()
        {
            var results = ReflectionHelpers.FindClasses<ClassTestAttribute1>(GetCurrentAssembly());
            Assert.NotNull(results);
            Assert.Equal(2, results.Keys.Count);
            Assert.True(results.ContainsKey(typeof(ClassWithAttribute1)));
            Assert.True(results.ContainsKey(typeof(ClassWithAttribute1AndAttribute2)));
        }

        /// <summary>
        /// Make sure we can find all classes with TestAttribute2
        /// </summary>
        [Fact]
        public void WillFindAllClassesWithAttribute2()
        {
            var results = ReflectionHelpers.FindClasses<ClassTestAttribute2>(GetCurrentAssembly());
            Assert.NotNull(results);
            Assert.Equal(2, results.Keys.Count);
            Assert.True(results.ContainsKey(typeof(ClassWithAttribute2)));
            Assert.True(results.ContainsKey(typeof(ClassWithAttribute1AndAttribute2)));
        }

        /// <summary>
        /// If no classes are marked with the requested attribute we get a non-null but
        /// empty result.
        /// </summary>
        [Fact]
        public void WillReturnEmptyResultIfNoMarkedClasses()
        {
            var results = ReflectionHelpers.FindClasses<ClassTestAttribute3>(GetCurrentAssembly());
            Assert.NotNull(results);
            Assert.Empty(results);
        }
    }
}
