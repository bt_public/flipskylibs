﻿namespace FlipSky.AppEngine
{
    /// <summary>
    /// General interface to WorkFlow extensions
    /// </summary>
    public interface IExtension : IConfigurable
    {
        /// <summary>
        /// Perform major initialisation
        /// </summary>
        void Initialise();
    }
}
