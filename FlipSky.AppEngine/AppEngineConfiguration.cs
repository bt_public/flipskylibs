﻿using System.Collections.Generic;

namespace FlipSky.AppEngine
{
    /// <summary>
    /// Configuration information for the AppEngine
    /// 
    /// This defines the injections and extensions that should be loaded
    /// when the AppEngine instance is initialised.
    /// </summary>
    public class AppEngineConfiguration
    {
        /// <summary>
        /// The injections to load
        /// </summary>
        public IEnumerable<InjectionConfiguration> Injections { get; set; }

        /// <summary>
        /// The extensions to load
        /// </summary>
        public IEnumerable<ExtensionConfiguration> Extensions { get; set; }
    }
}
