﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using FlipSky.Ensures;
using FlipSky.Logging;
using FlipSky.Depends;

namespace FlipSky.AppEngine
{
    /// <summary>
    /// Delegate for injection registered event
    /// </summary>
    public delegate void InjectionRegisteredHandler(AppEngine source, IResolver resolver, Type iface, Lifetime lifetime, ExtensionConfiguration extension);

    /// <summary>
    /// Delegate for extension loaded event
    /// </summary>
    public delegate void ExtensionLoadedHandler(AppEngine source, IExtension extension);

    /// <summary>
    /// The main AppEngine class
    /// </summary>
    public class AppEngine
    {
        /// <summary>
        /// This event is fired when an inject has been registered
        /// </summary>
        public event InjectionRegisteredHandler InjectionRegisteredEvent;

        /// <summary>
        /// This event is fired when an extension has been loaded and configured
        /// </summary>
        public event ExtensionLoadedHandler ExtensionLoadedEvent;

        /// <summary>
        /// The resolver to use to create classes
        /// </summary>
        private readonly IResolver Resolver;

        /// <summary>
        /// The configuration for this instance of the app engine
        /// </summary>
        private readonly AppEngineConfiguration Configuration;

        /// <summary>
        /// The logger to use
        /// </summary>
        private ILogger Log;

        /// <summary>
        /// Constructor
        /// </summary>
        public AppEngine(IResolver resolver, AppEngineConfiguration configuration, ILogger logger = null)
        {
            Resolver = resolver;
            Configuration = configuration;
            Log = logger ?? Resolver.Resolve<ILogger>();
        }

        /// <summary>
        /// Initialise the app engine. This will create all global injections then
        /// create and configure all extensions.
        /// </summary>
        public void Initialise()
        {
            Log.Info("Registering global injections");
            if (Configuration.Injections != null)
                foreach (var injection in Configuration.Injections)
                    RegisterInjection(Resolver, injection);
            // Switch loggers if a different one was injected
            var injectedLogger = Resolver.Resolve<ILogger>();
            if (injectedLogger != Log)
            {
                Log.Info("Switching to injected logger.");
                Log = injectedLogger;
            }
            // Do extensions in two passes - one to create, the other to configure
            if ((Configuration.Extensions == null) || !Configuration.Extensions.Any())
                Log.Warn("There are no extensions configured, this is probably not intended.");
            else
            {
                Log.Info("Creating extensions");
                var instances = Configuration.Extensions.Zip(
                    Configuration.Extensions.Select(i => LoadExtension(i)),
                    (k, v) => new { k, v })
                    .ToDictionary(x => x.k, x => x.v);
                Log.Info("Configuring extensions");
                foreach (var extension in Configuration.Extensions)
                    ConfigureExtension(instances[extension], extension);
                Log.Info("Initialising extensions");
                foreach (var extension in Configuration.Extensions)
                    instances[extension].Initialise();
            }
            Log.Info("AppEngine initialisation complete.");
        }

        #region Helpers
        /// <summary>
        /// Register an extension with the given resolver.
        /// </summary>
        private void RegisterInjection(IResolver resolver, InjectionConfiguration injection, ExtensionConfiguration extension = null)
        {
            Log.Info("Adding injection for '{0}' implemented by '{1}'", injection.Interface, injection.Implementation);
            // Get the interface and implementing type
            var iface = Type.GetType(injection.Interface, true);
            var impl = Type.GetType(injection.Implementation, true);
            Ensure.IsTrue(iface.IsAssignableFrom(impl));
            // Register a creator
            if (typeof(IConfigurable).IsAssignableFrom(impl))
            {
                resolver.Register(
                    iface,
                    (IResolver r) =>
                    {
                        var result = r.Resolve(impl);
                        (result as IConfigurable).Configure(injection.Configuration as JObject);
                        return result;
                    },
                    injection.Lifetime
                    );
            }
            else
                resolver.Register(iface, impl, injection.Lifetime);
            // Fire off the event
            InjectionRegisteredEvent?.Invoke(this, resolver, iface, injection.Lifetime, extension);
        }

        /// <summary>
        /// Create an extension instance.
        /// </summary>
        private IExtension LoadExtension(ExtensionConfiguration extension)
        {
            Log.Info("Initialising extension from class '{0}'", extension.Implementation);
            var resolver = Resolver;
            if ((extension.Injections != null) && (extension.Injections.Count > 0))
            {
                resolver = resolver.CreateChild();
                foreach (var injection in extension.Injections)
                    RegisterInjection(resolver, injection, extension);
            }
            // Get the class to create
            var t = Type.GetType(extension.Implementation, true);
            Ensure.IsTrue(typeof(IExtension).IsAssignableFrom(t));
            // Create it
            return resolver.Resolve(t) as IExtension;
        }

        /// <summary>
        /// Apply extension configuration
        /// </summary>
        private void ConfigureExtension(IExtension instance, ExtensionConfiguration extension)
        {
            Log.Info("Configuring extension '{0}'", extension.Implementation);
            instance.Configure(extension.Configuration as JObject);
            ExtensionLoadedEvent?.Invoke(this, instance);
        }
        #endregion
    }
}
