﻿using System.Collections.Generic;

namespace FlipSky.AppEngine
{
    /// <summary>
    /// Configuration information to load an extension
    /// </summary>
    public class ExtensionConfiguration
    {
        /// <summary>
        /// True if the extension is enabled.
        /// 
        /// This provides a 'kill switch' function - you can disable the
        /// extension without removing the configuration.
        /// </summary>
        public bool Enabled { get; set; } = true;

        /// <summary>
        /// The fully qualified name of the class implementing the extension
        /// </summary>
        public string Implementation { get; set; }

        /// <summary>
        /// The (optional) set of injects to apply to this extension
        /// </summary>
        public List<InjectionConfiguration> Injections { get; set; }

        /// <summary>
        /// The extension specific configuration information
        /// </summary>
        public object Configuration { get; set; }
    }
}
