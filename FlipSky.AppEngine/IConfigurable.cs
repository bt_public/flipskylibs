﻿using Newtonsoft.Json.Linq;

namespace FlipSky.AppEngine
{
    /// <summary>
    /// Support this interface to indicate a class can be configured
    /// from a JSON object.
    /// </summary>
    public interface IConfigurable
    {
        /// <summary>
        /// Perform configuration
        /// </summary>
        /// <param name="configuration"></param>
        void Configure(JObject configuration);
    }
}
