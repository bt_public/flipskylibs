﻿using FlipSky.Depends;

namespace FlipSky.AppEngine
{
    /// <summary>
    /// Configuration information to load an injection
    /// </summary>
    public class InjectionConfiguration
    {
        /// <summary>
        /// The (fully qualified) name of the interface to inject
        /// </summary>
        public string Interface { get; set; }

        /// <summary>
        /// The (fully qualified) name of the implementing class
        /// </summary>
        public string Implementation { get; set; }

        /// <summary>
        /// Lifetime of the object (default is Transient)
        /// </summary>
        public Lifetime Lifetime { get; set; } = Lifetime.Transient;

        /// <summary>
        /// The (optional) configuration information for the injection.
        /// 
        /// This will only be applied if the resulting instance implements
        /// the Bob.Core.IConfigurable interface.
        /// </summary>
        public object Configuration { get; set; }
    }
}
