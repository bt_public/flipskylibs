﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using FlipSky.Ensures;

namespace FlipSky.Strings
{
    /// <summary>
    /// Extension and utility methods to test and modify strings
    /// </summary>
    public static class StringHelpers
    {
        //--- Regular expressions for string matching
        private static readonly Regex MatchIdentifier = new Regex(@"^[a-zA-Z]\w*$");
        private static readonly Regex MatchIdentifierWithDash = new Regex(@"^[a-zA-Z][\w-]*$");
        private static readonly Regex MatchSingleWord = new Regex(@"^\S+$");
        private static readonly Regex MatchWord = new Regex(@"\S+");
        private static readonly Regex MatchVersion = new Regex(@"^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*))?$");
        private static readonly Regex MatchVersionWithSuffix = new Regex(@"^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*))?(-\w\S+)?$");

        /// <summary>
        /// Determine if the string is a valid identifier optionally allowing dashes
        /// </summary>
        public static bool IsIdentifier(this string value, bool allowDashes = false)
        {
            return
                (value != null) &&
                (allowDashes ? MatchIdentifierWithDash : MatchIdentifier).IsMatch(value);
        }

        /// <summary>
        /// Determine if the string is a single word with no embedded whitespace
        /// </summary>
        public static bool IsSingleWord(this string value)
        {
            return
                (value != null) &&
                MatchSingleWord.IsMatch(value);
        }

        /// <summary>
        /// Determine if the string is a valid version number (2 or 3 components) optionally
        /// allowing a free text suffix
        /// </summary>
        public static bool IsVersionNumber(this string value, bool allowSuffix = false)
        {
            return
                (value != null) &&
                (allowSuffix ? MatchVersionWithSuffix : MatchVersion).IsMatch(value);
        }

        /// <summary>
        /// Iterator over all words in the string
        /// </summary>
        public static IEnumerable<string> SplitIntoWords(this string value)
        {
            var match = MatchWord.Match(value);
            while (match.Success)
            {
                yield return match.Value;
                match = match.NextMatch();
            }
        }

        /// <summary>
        /// Layout a block of text to the specified with (in characters) with
        /// optional space padding in the start. Returns a string (with embedded
        /// newlines) that can be displayed.
        /// </summary>
        public static string LayoutText(string source, int width, int padding = 0, string eol = "\n")
        {
            Ensure.IsNotNull<ArgumentException>(eol, "End of line sequence must be specified.");
            Ensure.IsTrue<ArgumentException>(width > 0, "Content width cannot be zero or negative");
            Ensure.IsTrue<ArgumentException>(padding >= 0, "Padding cannot be negative");
            Ensure.IsTrue<ArgumentException>((width - padding) > 0, "Total content size cannot be zero or negative");
            // Start the layout
            var result = new StringBuilder();
            int availableSpace = width - padding;
            int line = 0;
            foreach (var word in source.SplitIntoWords())
            {
                if (word.Length > availableSpace)
                    throw new InvalidOperationException("Cannot layout text as requested.");
                // Do we need to start a new line?
                if (line + word.Length + ((line > 0) ? 1 : 0) > width)
                {
                    result.Append(eol);
                    line = 0;
                }
                // Are we starting a new line?
                if (line == 0)
                {
                    result.Append("".PadLeft(padding));
                    result.Append(word);
                    line = padding + word.Length;
                }
                else
                {
                    result.Append(" ");
                    result.Append(word);
                    line = line + word.Length + 1;
                }
            }
            // And we are done
            return result.ToString();
        }
    }
}
