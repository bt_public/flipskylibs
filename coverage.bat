@echo off
dotnet test
reportgenerator -reports:coverage/*.xml -targetdir:coverage/html -reporttypes:html
reportgenerator -reports:coverage/*.xml -targetdir:coverage/badges -reporttypes:badges
