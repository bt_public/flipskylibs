﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;

namespace FlipSky.Mapper.Test
{
    /// <summary>
    /// Basic mapping tests
    /// </summary>
    public class SimpleMappingTests
    {
        /// <summary>
        /// If the objects are the same type a copy will not be made by default
        /// </summary>
        [Fact]
        public void WillMapWithoutCloning()
        {
            var mapper = new TestModelMapper();
            var source = new TestModel() { Field1 = "hello", Field2 = 42 };
            var target = mapper.Map(source);
            Assert.True(ReferenceEquals(source, target));
        }

        /// <summary>
        /// If 'allowClones' is true a copy will be made of the source
        /// </summary>
        [Fact]
        public void WillMapWithCloning()
        {
            var mapper = new TestModelMapperWithCloning();
            var source = new TestModel() { Field1 = "hello", Field2 = 42 };
            var target = mapper.Map(source);
            Assert.False(ReferenceEquals(source, target));
            Assert.Equal(source.Field1, target.Field1);
            Assert.Equal(source.Field2, target.Field2);
        }

        /// <summary>
        /// Will use a custom factory to create
        /// </summary>
        [Fact]
        public void WillCloneWithCustomFactory()
        {
            // Set up a custom mapper mock
            var mock = new Mock<IMapper<ITestModel, TestModel>>();
            mock.Setup(m => m.CreateInstance(It.IsAny<ITestModel>())).Returns((TestModel)null);
            mock.Setup(m => m.Map(It.IsAny<ITestModel>(), It.IsAny<TestModel>()));
            // Do a mapping from one type to another
            var source = new TestModel() { Field1 = "hello", Field2 = 42 };
            var target = mock.Object.Map(source);
            Assert.False(ReferenceEquals(source, target));
            mock.Verify(factory => factory.CreateInstance(It.IsAny<ITestModel>()), Times.Once);
        }

        /// <summary>
        /// The default implementation will map null values without error
        /// </summary>
        [Fact]
        public void DefaultMapperWillMapNullToNull() 
        {
            var mapper = new TestModelMapper();
            var result = mapper.Map((ITestModel)null);
            Assert.Null(result);
        }
    }
}
