﻿namespace FlipSky.Mapper.Test
{
    /// <summary>
    /// A simple read only model
    /// </summary>
    public interface ITestModel
    {
        string Field1 { get; }

        int Field2 { get; }
    }

    /// <summary>
    /// A concrete version of the model
    /// </summary>
    public class TestModel : ITestModel
    {
        public string Field1 { get; set; }

        public int Field2 { get; set; }
    }

    /// <summary>
    /// Mapper from abstraction to concrete
    /// </summary>
    public class TestModelMapper : AbstractMapper<ITestModel, TestModel>
    {
        public override void Map(ITestModel source, TestModel target)
        {
            target.Field1 = source.Field1;
            target.Field2 = source.Field2;
        }
    }

    /// <summary>
    /// Mapper from abstraction to concrete with cloning
    /// </summary>
    public class TestModelMapperWithCloning : AbstractMapper<ITestModel, TestModel>
    {
        /// <summary>
        /// Constructor with cloning enabled
        /// </summary>
        public TestModelMapperWithCloning() : base(doNotClone: false) { }

        public override void Map(ITestModel source, TestModel target)
        {
            target.Field1 = source.Field1;
            target.Field2 = source.Field2;
        }
    }

}
