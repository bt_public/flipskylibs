﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;

namespace FlipSky.Mapper.Test
{
    public class CollectionMappingTests
    {
        /// <summary>
        /// Number of instances to use in the source data
        /// </summary>
        private const int InstanceCount = 5;

        /// <summary>
        /// Create data to test against
        /// </summary>
        private IEnumerable<ITestModel> CreateSourceData()
        {
            return Enumerable.Range(0, InstanceCount)
                .Select(m => new TestModel() { Field1 = m.ToString(), Field2 = m });
        }

        /// <summary>
        /// If the objects are the same type a copy will not be made by default
        /// </summary>
        [Fact]
        public void WillMapWithoutCloning()
        {
            var mapper = new TestModelMapper();
            var source = CreateSourceData();
            var target = mapper.Map(source);
            // Quick way to test each item
            int count = source.Zip(target, (s, t) => { Assert.False(ReferenceEquals(s, t)); return true; }).Count();
            Assert.Equal(InstanceCount, count); // Not needed but forces evaluation
        }

        /// <summary>
        /// Will map a List<>
        /// </summary>
        [Fact]
        public void WillMapListTypes()
        {
            var mapper = new TestModelMapper();
            var source = CreateSourceData().ToList();
            IList<TestModel> target = mapper.Map(source);
            // Quick way to test each item
            int count = source.Select(o => { Assert.NotNull(0); return o; }).Count();
            Assert.Equal(InstanceCount, count); // Not needed but forces evaluation
        }

        /// <summary>
        /// Will map an array
        /// </summary>
        [Fact]
        public void WillMapArrayTypes()
        {
            var mapper = new TestModelMapper();
            var source = CreateSourceData().ToArray();
            TestModel[] target = mapper.Map(source);
            // Quick way to test each item
            int count = source.Select(o => { Assert.NotNull(0); return o; }).Count();
            Assert.Equal(InstanceCount, count); // Not needed but forces evaluation
        }

        /// <summary>
        /// If 'allowClones' is true a copy will be made of the source
        /// </summary>
        [Fact]
        public void WillMapWithCloning()
        {
            var mapper = new TestModelMapperWithCloning();
            var source = CreateSourceData();
            var target = mapper.Map(source);
            // Quick way to test
            int count = source.Zip(target, (s, t) => { Assert.False(ReferenceEquals(s, t)); return true; }).Count();
            Assert.Equal(InstanceCount, count); // Not needed but forces evaluation
        }

        /// <summary>
        /// Will use a custom factory to create
        /// </summary>
        [Fact]
        public void WillUseCustomFactory()
        {
            // Set up a custom mapper mock
            var mock = new Mock<IMapper<ITestModel, TestModel>>();
            mock.Setup(m => m.CreateInstance(It.IsAny<ITestModel>())).Returns((TestModel)null);
            mock.Setup(m => m.Map(It.IsAny<ITestModel>(), It.IsAny<TestModel>()));
            // Do a mapping from one type to another
            var source = CreateSourceData();
            var target = mock.Object.Map(source);
            // Quick way to test equality
            int count = source.Zip(target, (s, t) => { Assert.False(ReferenceEquals(s, t)); return true; }).Count();
            Assert.Equal(InstanceCount, count); // Not needed but forces evaluation
            // Make sure we didn't call the custom factory
            mock.Verify(m => m.CreateInstance(It.IsAny<ITestModel>()), Times.Exactly(InstanceCount));
        }
    }
}
