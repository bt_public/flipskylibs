#!/bin/bash
#----------------------------------------------------------------------------
# Build script.
#
# Use a single script so we can run everything in a single container
#----------------------------------------------------------------------------

# Versioning
VERSION_PREFIX=3.2
VERSION_SUFFIX=-preview

# Target
TARGET_CONFIG=Debug

#--- Set environment based on parameter
if [ "X$1" == "Xdeploy" ]; then
  TARGET_CONFIG=Release
  if [ "X${CI_COMMIT_REF_NAME}" == "Xrelease" ]; then
    VERSION_SUFFIX=
  fi
fi

#--- Build the full version number
VERSION=${VERSION_PREFIX}.${CI_PIPELINE_IID}${VERSION_SUFFIX}
echo "*** Buiding version ${VERSION}/${TARGET_CONFIG}"


# Build and test
dotnet build --configuration ${TARGET_CONFIG} -p:Version=${VERSION} || exit 1
dotnet test --configuration ${TARGET_CONFIG} -p:Version=${VERSION} || exit 1

echo "*** Deploying for target ${TARGET_CONFIG}"

if [ "X${TARGET_CONFIG}" == "XRelease" ]; then
  if [ "X${NUGET_KEY}" == "X" ]; then
    echo "ERROR: No NuGet deploy key provided"
    exit 1
  fi

  # Build NuGet Packages
  PKGDIR=$(pwd)/nupkg
  mkdir -p ${PKGDIR}
  dotnet pack --configuration Release -p:Version=${VERSION} --output ${PKGDIR}
  echo "Packages are:"
  ls ${PKGDIR}/*.nupkg
  echo "Publishing ..."
  for pkg in `ls ${PKGDIR}/*.nupkg`; do
    dotnet nuget push ${pkg} -k ${NUGET_KEY} -s https://api.nuget.org/v3/index.json || exit 1
  done
else
  echo "Target configuration is not Release, no deployment for you."
fi
