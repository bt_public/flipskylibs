﻿using System;
using System.Linq;
using Xunit;

namespace FlipSky.Topics.Test
{
    /// <summary>
    /// Test topic parsing and wildcard matching
    /// </summary>
    public class TopicTests
    {
        /// <summary>
        /// Used to hold test data for topic matching
        /// </summary>
        class TopicMatchRequirements
        {
            public string Topic;
            public bool[] Matches;
        }

        /// <summary>
        /// Check for correct validation of topic names
        /// </summary>
        [Fact]
        public void TopicValidity()
        {
            // These should all be valid
            Assert.True(Topic.IsValid("level1"));                     // Single level topic
            Assert.True(Topic.IsValid("level1/level2"));              // Multilevel topic
            Assert.True(Topic.IsValid("level1/level2/level3"));       // Multilevel topic
            Assert.True(Topic.IsValid("+/level2"));                   // Start with wildcard
            Assert.True(Topic.IsValid("level1/+/level2"));            // Wildcard in body
            Assert.True(Topic.IsValid("level1/+"));                   // End with wildcard
            Assert.True(Topic.IsValid("+/level2/+"));                 // Multiple wildcards (at ends)
            Assert.True(Topic.IsValid("level1/+/level2/+/level3"));   // Multiple wildcards (in body)
            Assert.True(Topic.IsValid("#"));                          // Just the 'rest' wildcard
            Assert.True(Topic.IsValid("level1/#"));                   // Multilevel with 'rest' wildcard
            Assert.True(Topic.IsValid("level1/level2/#"));            // Multilevel with 'rest' wildcard
            // These should all be invalid
            Assert.False(Topic.IsValid(""));                          // Empty topic
            Assert.False(Topic.IsValid("/level1"));                   // Empty part
            Assert.False(Topic.IsValid("level1//level2"));            // Empty part
            Assert.False(Topic.IsValid("#/level2"));                  // 'rest' wildcard not at end
            Assert.False(Topic.IsValid("level1/#/level2"));           // 'rest' wildcard not at end
        }

        /// <summary>
        /// Check for splitting topics into parts
        /// </summary>
        [Fact]
        public void TopicSplitting()
        {
            string[] source = new string[] { "level1", "level2", "level3", "level4" };
            for (int i = 1; i < source.Length; i++)
            {
                string topic = String.Join("/", source.Take(i));
                var parts = Topic.Parse(topic);
                for (int j = 0; j < i; j++)
                    Assert.Equal(source[j], parts[j]);
            }
        }

        [Theory]
        [InlineData("level1", false, false, false, false)]
        [InlineData("level1/level2", false, true, false, true)]
        [InlineData("level1/level2/level3", true, true, true, false)]
        [InlineData("level1/child", false, false, false, false)]
        [InlineData("level1/child/level3", false, false, true, false)]
        [InlineData("level1/level2/child", false, true, false, false)]
        [InlineData("child", false, false, false, false)]
        [InlineData("child/level2", false, false, false, true)]
        [InlineData("child/level2/level3", false, false, false, false)]
        public void TopicMatching(string topic, params bool[] shouldMatch)
        {
            // Subscription patterns to test
            string[] subscriptions = new string[]
            {
                "level1/level2/level3",
                "level1/level2/#",
                "level1/+/level3",
                "+/level2"
            };
            // Now do the comparison
            for (int i = 0; i < subscriptions.Length; i++)
                Assert.Equal(shouldMatch[i], Topic.MatchesSubscription(subscriptions[i], topic));
        }

        /// <summary>
        /// Test topic matching with invalid and null topics
        /// </summary>
        [Fact]
        public void TopicMatchingInvalid()
        {
            // Test invalid topic combinations
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription("", "topic"); });
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription("topic", ""); });
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription("", ""); });
            // Test null topic combinations
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription(null, "topic"); });
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription("topic", null); });
            Assert.Throws<ArgumentException>(() => { Topic.MatchesSubscription(null, null); });
        }
    }
}
