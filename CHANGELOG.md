# Recent Changes

## Mar-22-2019 Added help text generation for FlipSky.Options

The OptionParser can no generate help text for the list of defined options (only the options, the rest of the help text for the command line is up to you).

## Mar-21-2019 Added FlipSky.Options library

Added a library to support parsing GNU style command line options. This is intended as a replacement for NDesk.Options which works great but is built for NET4.6 rather than NetStandard.

## Mar-20-2019 Added FlipSky.Strings library

This library provides simple format checking (versions, single words, identifiers, etc) as well as a helper method to layout a block of text to fit a specific screen width.
