using System;
using System.Linq;
using FlipSky.Ensures;

namespace FlipSky.Topics
{
    /// <summary>
    /// Represents a single message topic. In the messaging framework we allow for MQTT
    /// style message topics as a way to route messages. This class provides helpers to
    /// test, verify and match those topics
    /// </summary>
    public class Topic
    {
        /// <summary>
        /// Wildcard character to match a single subtopic
        /// </summary>
        public const string MatchSingleSubtopic = "+";

        /// <summary>
        /// Wildcard to match all remaining subtopics
        /// </summary>
        public const string MatchAllSubtopics = "#";

        /// <summary>
        /// Character used to separate subtopics
        /// </summary>
        public const char Separator = '/';

        /// <summary>
        /// The component parts of the topic
        /// </summary>
        private string[] m_components;

        /// <summary>
        /// The actual value of the topic
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// If the topic contains wildcards this will be set to true
        /// </summary>
        public bool ContainsWildcards { get; private set; }

        /// <summary>
        /// Access the component parts of the topic
        /// </summary>
        public string[] Components
        {
            get
            {
                // Return a defensive copy
                var result = new string[m_components.Length];
                m_components.CopyTo(result, 0);
                return result;
            }
        }

        /// <summary>
        /// Constructor with a topic string
        /// </summary>
        /// <param name="topic"></param>
        public Topic(string topic)
        {
            // Split it up (will also verify)
            m_components = Parse(topic);
            Value = topic;
            // Do we have wildcards
            ContainsWildcards = m_components.Contains(MatchSingleSubtopic) || m_components.Contains(MatchAllSubtopics);
        }

        /// <summary>
        /// Determine if the given topic matches this one
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public bool Matches(Topic topic)
        {
            Ensure.IsNotNull(topic);
            return MatchesSubscription(m_components, topic.m_components);
        }

        /// <summary>
        /// Determine if the given topic matches this one
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public bool Matches(string topic)
        {
            var components = Parse(topic);
            return MatchesSubscription(m_components, components);
        }

        /// <summary>
        /// Append a topic to this one and return a new topic
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public Topic Append(Topic topic)
        {
            Ensure.IsNotNull(topic);
            return new Topic($"{this.ToString()}/{topic.ToString()}");
        }

        /// <summary>
        /// Append a topic to this one and return a new topic
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public Topic Append(string topic)
        {
            Ensure.IsNotNull(topic);
            return new Topic($"{this.ToString()}/{topic}");
        }

        /// <summary>
        /// When converting to a string use the original topic
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Value;
        }

        /// <summary>
        /// Hashcodes are based on the original string
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        /// Equality is based on the original string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var other = obj as Topic;
            return (other != null) && (other.Value == Value);
        }

        /// <summary>
        /// Verify the topic and split it into an array of parts
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public static string[] Parse(string topic)
        {
            Ensure.IsNotNullOrWhiteSpace(topic);
            // Split into parts and verify
            var components = topic.Split(Separator);
            for (int i = 0; i < components.Length; i++)
            {
                Ensure.IsNotNullOrWhiteSpace(components[i]);
                Ensure.IsTrue(components[i].Trim().Length == components[i].Length);
                // Check for 'magic' parts
                if ((components[i] == MatchAllSubtopics) && (i != (components.Length - 1)))
                    throw new ArgumentException();
            }
            return components;
        }

        /// <summary>
        /// Verify the topic and fill out a list of parts
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static bool IsValid(string topic)
        {
            try
            {
                Parse(topic);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Helper to match two topics (allowing for wildcards)
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="topic"></param>
        /// <returns></returns>
        private static bool MatchesSubscription(string[] subscription, string[] topic)
        {
            for (int i = 0; i < topic.Length; i++)
            {
                if (i >= subscription.Length)
                    return false;
                if (subscription[i] == MatchAllSubtopics)
                    return true;
                if ((topic[i] != subscription[i]) && (subscription[i] != MatchSingleSubtopic))
                    return false;
            }
            // Same length means it's a match
            if (topic.Length == subscription.Length)
                return true;
            // If the next part is 'match all' we have a match
            if ((topic.Length == (subscription.Length - 1)) && (subscription[subscription.Length - 1] == MatchAllSubtopics))
                return true;
            // No match
            return false;
        }

        /// <summary>
        /// Match the topic against the subscription. This supports MQTT
        /// style wildcards (+ and #)
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static bool MatchesSubscription(string subscription, string topic)
        {
            var topicParts = Parse(topic);
            var subParts = Parse(subscription);
            return MatchesSubscription(subParts, topicParts);
        }
    }
}
